#!/usr/bin/env python
#change to plot from excel table
import glob
import sys
import csv
import numpy as np
import os
import copy
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import json
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

plt.style.use('seaborn-whitegrid')
col = plt.rcParams['axes.prop_cycle']

# plot the param study data
fig = plt.figure(dpi=600, figsize=(11,8))

font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 14}

font2 = {'family' : 'normal',
         'weight' : 'bold',
         'size'   : 14}

plt.rc('font', **font)


plt.title('HDO concentration in the porous medium\n', fontsize=font['size'], fontweight=font['weight'])

# this can be probably solved also with some loops
data_path = '../Experiment/Dumux/isotope_profile/day289.csv'
with open(data_path, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data = np.array(list(reader)).astype(float)

data_path2 = '../Experiment/SiSPAT/isotope_profile/day289.csv'
with open(data_path2, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data2 = np.array(list(reader)).astype(float)
    
data_path3 = '../Experiment/Experiment/isotope_profile/day289.csv'
with open(data_path3, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data3 = np.array(list(reader)).astype(float)




yCoordinate_Dumux = data[:,40]*-0.001
yCoordinate_Dumux = yCoordinate_Dumux[::-1]
Dumux = data[:,27]

yCoordinate_Sispat = data2[:,0]
Sispat = data2[:,1]

yCoordinate_Experiments = data3[:,1]
Experiments = data3[:,4]

Dumux_1 = data[:,26]
Sispat_1 = data2[:,2]
Experiments_1 = data3[:,3]


fig, (axs1, axs2) = plt.subplots(1,2, sharex=False, sharey=True)
#fig, (axs1) = plt.subplots(1,1, sharex=False, sharey=True)

axs1.plot(Dumux, yCoordinate_Dumux, linestyle='-', linewidth=2, label='DuMu$^x$')
axs1.plot(Sispat, yCoordinate_Sispat, linestyle='-.', linewidth=2, label='SiSPAT-isotope')
axs1.plot(Experiments, yCoordinate_Experiments,'xk', label='Experiments')


axs2.plot(Dumux_1, yCoordinate_Dumux, linestyle='-', linewidth=2, label='DuMu$^x$')
axs2.plot(Sispat_1, yCoordinate_Sispat, linestyle='-.', linewidth=2, label='SiSPAT-Isotope')
axs2.plot(Experiments_1, yCoordinate_Experiments, 'xk', label='Experiment')


#axs1.set_ylim([-0.018, 0.0])

#axs1.set_title('HDO')
#axs2.set_title('H2O18')

# Set common label for depth
fig.text(0.02, 0.5, 'Depth [m]', fontsize=font['size'], fontweight=font['weight'], ha='center', va='center', rotation='vertical')

fig.text(0.5, 0.95, 'c) Day 289', fontsize=font['size'], fontweight=font['weight'], ha='center', va='center', rotation='horizontal')

#set x-labels
#axs1.set_xlabel("Saturation [m$^{3}$m$^{-3}$]", fontsize=font['size'], fontweight=font['weight'])
axs2.set_xlabel(u'$\delta^{^{18}O}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'])
axs1.set_xlabel(u'$\delta^{^2H}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'])


axs2.legend(loc='lower right', frameon=True)
#fig.tight_layout(pad=2, w_pad=1.0, h_pad=1.0)


# place a text box in upper left in axes coords
# these are matplotlib.patch.Patch properties
#textstr = 'a)\n'
#ax.text(-1.5, 1.08, textstr, transform=ax.transAxes, fontsize=font['size'], fontweight=font['weight'], verticalalignment='top')

plt.show()
fig.savefig("HDO_isotopeprofile_Day_289.png")
fig.savefig("HDO_isotopeprofile_Day_289.pdf")
