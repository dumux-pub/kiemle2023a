#!/usr/bin/env python
#change to plot from excel table
import glob
import sys
import csv
import numpy as np
import os
import copy
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import json
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

plt.style.use('seaborn-whitegrid')
col = plt.rcParams['axes.prop_cycle']



font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 14}

font2 = {'family' : 'normal',
         'weight' : 'bold',
         'size'   : 14}

plt.rc('font', **font)


# this can be probably solved also with some loops
data_path = '../Experiment/Dumux/isotope_profile/day1.csv'
with open(data_path, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data = np.array(list(reader)).astype(float)
    

data_path2 = '../Experiment/Dumux/isotope_profile/day11.csv'
with open(data_path2, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data2 = np.array(list(reader)).astype(float)
    
data_path3 = '../Experiment/Dumux/isotope_profile/day22.csv'
with open(data_path3, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data3 = np.array(list(reader)).astype(float)
    
data_path4 = '../Experiment/Dumux/isotope_profile/day6.csv'
with open(data_path4, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data4 = np.array(list(reader)).astype(float)   
    
data_path5 = '../Experiment/Dumux/isotope_profile/day140.csv'
with open(data_path5, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data5 = np.array(list(reader)).astype(float)   
    

data_path6 = '../Experiment/Dumux/isotope_profile/day289.csv'
with open(data_path6, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data6 = np.array(list(reader)).astype(float)     
    
data_path7 = '../Experiment/Dumux/evaporation_rate_hourly.csv'
with open(data_path7, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data7 = np.array(list(reader)).astype(float)
              




yCoordinate_Dumux = data2[:,1]*-1
yCoordinate_Dumux = yCoordinate_Dumux[::-1]

yCoordinate_Dumux_2 = data5[:,40]*-0.001
yCoordinate_Dumux_2 = yCoordinate_Dumux_2[::-1]




Dumux_day1_HDO = data[:,27]
Dumux_day6_HDO = data4[:,18]
Dumux_day11_HDO = data2[:,18]
Dumux_day22_HDO = data3[:,18]
Dumux_day140_HDO = data5[:,27]
Dumux_day289_HDO = data6[:,27]

Dumux_day1_HO18 = data[:,26]
Dumux_day6_HO18 = data4[:,17]
Dumux_day11_HO18 = data2[:,17]
Dumux_day22_HO18 = data3[:,17]
Dumux_day140_HO18 = data5[:,26]
Dumux_day289_HO18 = data6[:,26]


yCoordinate_Dumux_Evap = data7[:,1]
Dumux_Evap = data7[:,0]/24

fig, (axs1, axs2, axs3) = plt.subplots(1, 3, sharex=False, sharey=False, figsize=(12, 6), gridspec_kw={'wspace': 0.3})


axs2.plot(Dumux_day1_HDO, yCoordinate_Dumux_2, linestyle='-', linewidth=2, label='1 day')
axs2.plot(Dumux_day6_HDO, yCoordinate_Dumux, linestyle='-', color='cyan', alpha=0.6, linewidth=2, label='6 days')
axs2.plot(Dumux_day11_HDO, yCoordinate_Dumux, linestyle='-.', color='blue', alpha=0.6, linewidth=2, label='11 days')
axs2.plot(Dumux_day22_HDO, yCoordinate_Dumux, linestyle=':', color='darkblue', alpha=0.3, label='22 days')
axs2.plot(Dumux_day140_HDO, yCoordinate_Dumux_2, linestyle='-.', color='blue', label='140 days')
axs2.plot(Dumux_day289_HDO, yCoordinate_Dumux_2, linestyle='-', color='darkblue', label='289 days')

axs3.plot(Dumux_day1_HO18, yCoordinate_Dumux_2, linestyle='-', linewidth=2, label='1 day')
axs3.plot(Dumux_day6_HO18, yCoordinate_Dumux, linestyle='-', color='cyan', alpha=0.6, linewidth=2, label='6 days')
axs3.plot(Dumux_day11_HO18, yCoordinate_Dumux, linestyle='-.', linewidth=2, color='blue', alpha=0.6, label='11 days')
axs3.plot(Dumux_day22_HO18, yCoordinate_Dumux, linestyle=':', color='darkblue', alpha=0.3, label='22 days')
axs3.plot(Dumux_day140_HO18, yCoordinate_Dumux_2, linestyle='-.', color='blue', label='140 days')
axs3.plot(Dumux_day289_HO18, yCoordinate_Dumux_2, linestyle='-', color='darkblue', label='289 days')

axs1.axvline(x=1, color='g', linestyle='--', label='Day 1')
axs1.axvline(x=6, color='g', linestyle='--', label='Day 6')
axs1.axvline(x=11, color='g', linestyle='--', label='Day 11')
axs1.axvline(x=22, color='g', linestyle='--', label='Day 22')
axs1.axvline(x=140, color='g', linestyle='--', label='Day 140')
axs1.axvline(x=289, color='g', linestyle='--', label='Day 289')
axs1.plot(Dumux_Evap, yCoordinate_Dumux_Evap, linestyle='-', linewidth=2)

#zoom in
axins = inset_axes(axs2, width='60%', height='10%', loc=7)
axins_1 = inset_axes(axs3, width='60%', height='10%', loc=7)

axins.plot(Dumux_day1_HDO, yCoordinate_Dumux_2, linestyle='-', linewidth=2, label='1 day')
axins.plot(Dumux_day6_HDO, yCoordinate_Dumux, linestyle='-', color='cyan', alpha=0.6, linewidth=2, label='6 days')
axins.plot(Dumux_day11_HDO, yCoordinate_Dumux, linestyle='-.', color='blue', alpha=0.6, linewidth=2, label='11 days')
axins.plot(Dumux_day22_HDO, yCoordinate_Dumux, linestyle=':', color='darkblue', alpha=0.3, label='22 days')
axins.plot(Dumux_day140_HDO, yCoordinate_Dumux_2, linestyle='-.', color='blue', label='140 days')
axins.plot(Dumux_day289_HDO, yCoordinate_Dumux_2, linestyle='-', color='darkblue', label='289 days')


axins_1.plot(Dumux_day1_HO18, yCoordinate_Dumux_2, linestyle='-', linewidth=2, label='1 day')
axins_1.plot(Dumux_day6_HO18, yCoordinate_Dumux, linestyle='-', color='cyan', alpha=0.6, linewidth=2, label='6 days')
axins_1.plot(Dumux_day11_HO18, yCoordinate_Dumux, linestyle='-.', linewidth=2, color='blue', alpha=0.6, label='11 days')
axins_1.plot(Dumux_day22_HO18, yCoordinate_Dumux, linestyle=':', color='darkblue', alpha=0.3, label='22 days')
axins_1.plot(Dumux_day140_HO18, yCoordinate_Dumux_2, linestyle='-.', color='blue', label='140 days')
axins_1.plot(Dumux_day289_HO18, yCoordinate_Dumux_2, linestyle='-', color='darkblue', label='289 days')

axins.set_ylim(-0.02, 0.0) # Limit the region for zoom
axins.set_xlim(-53, 25)
axins.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
axins_1.set_ylim(-0.02, 0.0) # Limit the region for zoom
axins_1.set_xlim(-9, 16)
axins_1.ticklabel_format(style='sci', axis='y', scilimits=(0,0))


## draw a bbox of the region of the inset axes in the parent axes and
## connecting lines between the bbox and the inset axes area
mark_inset(axs2, axins, loc1=3, loc2=4, fc="none", ec="0.5")
mark_inset(axs3, axins_1, loc1=3, loc2=4, fc="none", ec="0.5")



axs2.set_ylim([-0.6, 0.0])
axs3.set_ylim([-0.6, 0.0])

#axs1.set_title('HDO')
#axs2.set_title('H2O18')

# Set common label for depth
fig.text(0.35, 0.5, 'Depth [m]', fontsize=font['size'], fontweight=font['weight'], ha='center', va='center', rotation='vertical')

#fig.text(0.5, 0.95, 'a) Day 1.5 (stage-I peak)', fontsize=font['size'], fontweight=font['weight'], ha='center', va='center', rotation='horizontal')

#set x-labels
#axs1.set_xlabel("Saturation [m$^{3}$m$^{-3}$]", fontsize=font['size'], fontweight=font['weight'])
axs3.set_xlabel(u'$\delta^{^{18}O}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'])
axs2.set_xlabel(u'$\delta^{^2H}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'])

axs1.set_ylabel('Evaporation rate [mm/h]', fontsize=font['size'], fontweight=font['weight'])
axs1.set_xlabel('Time [d]', fontsize=font['size'], fontweight=font['weight'])





#plt.legend(loc='upper center', bbox_to_anchor=(-0.72, -0.05), frameon=True, ncol=6)
# Place the legend at the top of the plots, outside the axes.
legend = plt.legend(loc='upper center', bbox_to_anchor=(0.83, 0.43), frameon=True, ncol=1, bbox_transform=fig.transFigure)

# place a text box in upper left in axes coords
# these are matplotlib.patch.Patch properties
textstr = 'a)\n'
axs1.text(0.01, 1.05, textstr, transform=axs1.transAxes, fontsize=font['size'], fontweight=font['weight'], verticalalignment='top')
textstr = 'b)\n'
axs2.text(1.35, 1.05, textstr, transform=axs1.transAxes, fontsize=font['size'], fontweight=font['weight'], verticalalignment='top')
textstr = 'c)\n'
axs3.text(2.68, 1.05, textstr, transform=axs1.transAxes, fontsize=font['size'], fontweight=font['weight'], verticalalignment='top')



# place a text box in upper left in axes coords
# these are matplotlib.patch.Patch properties
#textstr = 'a)\n'
#ax.text(-1.5, 1.08, textstr, transform=ax.transAxes, fontsize=font['size'], fontweight=font['weight'], verticalalignment='top')

plt.show()
fig.savefig("HDO_isotopeprofile_close.png")
fig.savefig("HDO_isotopeprofile_close.pdf")
