#!/usr/bin/env python
#change to plot from excel table
import glob
import sys
import csv
import numpy as np
import os
import copy
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import json
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

plt.style.use('seaborn-whitegrid')
col = plt.rcParams['axes.prop_cycle']


font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 14}

font2 = {'family' : 'normal',
         'weight' : 'bold',
         'size'   : 14}

plt.rc('font', **font)

# this can be probably solved also with some loops

 #Dumux 
data_path1 = '../Sensitivität/dumux/theta_r_000.csv'
with open(data_path1, 'r') as f:
    reader = csv.reader(f, delimiter=';')
    headers = next(reader)
    data1 = np.array(list(reader)).astype(float)

data_path2 = '../Sensitivität/dumux/theta_r_002.csv'
with open(data_path2, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data2 = np.array(list(reader)).astype(float)
    
data_path3 = '../Sensitivität/dumux/theta_r_004.csv'
with open(data_path3, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data3 = np.array(list(reader)).astype(float) 

data_path4 = '../Sensitivität/dumux/theta_r_006.csv'
with open(data_path4, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data4 = np.array(list(reader)).astype(float) 
    
data_path9 = '../Sensitivität/dumux/evaporation.csv'
with open(data_path9, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data9 = np.array(list(reader)).astype(float) 



 #Sispat 
data_path5 = '../Sensitivität/SiSPAT/theta_r_0/theta_r_0_590.csv'
with open(data_path5, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data5 = np.array(list(reader)).astype(float)

data_path6 = '../Sensitivität/SiSPAT/theta_r_0.02/theta_r_002_590.csv'
with open(data_path6, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data6 = np.array(list(reader)).astype(float)
    
data_path7 = '../Sensitivität/SiSPAT/theta_r_0.04/theta_r_004_590.csv'
with open(data_path7, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data7 = np.array(list(reader)).astype(float) 

data_path8 = '../Sensitivität/SiSPAT/theta_r_0.06/theta_r_006_590.csv'
with open(data_path8, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data8 = np.array(list(reader)).astype(float)  
    
 #Sispat evaporation data    
data_path13 = '../Sensitivität/SiSPAT/theta_r_0/theta_r_0_evaporation.csv'
with open(data_path13, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data13 = np.array(list(reader)).astype(float)

data_path10 = '../Sensitivität/SiSPAT/theta_r_0.02/theta_r_002_evaporation.csv'
with open(data_path10, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data10 = np.array(list(reader)).astype(float)
    
data_path11 = '../Sensitivität/SiSPAT/theta_r_0.04/theta_r_004_evaporation.csv'
with open(data_path11, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data11 = np.array(list(reader)).astype(float) 

data_path12 = '../Sensitivität/SiSPAT/theta_r_0.06/theta_r_006_evaporation.csv'
with open(data_path12, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data12 = np.array(list(reader)).astype(float)            
              

# Dumux
# Dumux is adapted for input data after 25 hours to match with SiSPAT input
Dumux_time = data1[126:,6]/3600/24
Dumux_theta_r_000_Oxygen = data1[126:,39]
Dumux_time_002 = data2[135:,6]/3600/24 
Dumux_theta_r_002_Oxygen = data2[135:,39]
Dumux_time_004 = data3[123:,6]/3600/24 
Dumux_theta_r_004_Oxygen = data3[123:,39]
Dumux_time_006 = data4[128:,6]/3600/24 
Dumux_theta_r_006_Oxygen = data4[128:,39]
Dumux_theta_r_000_Deuterium = data1[126:,40] 
Dumux_theta_r_002_Deuterium = data2[135:,40]
Dumux_theta_r_004_Deuterium = data3[123:,40]
Dumux_theta_r_006_Deuterium= data4[128:,40]
Dumux_theta_r_000_Sat = data1[126:,4] 
Dumux_theta_r_002_Sat = data2[135:,4]
Dumux_theta_r_004_Sat = data3[123:,4]
Dumux_theta_r_006_Sat = data4[128:,4]

Dumux_theta_r_000_Evap = data9[189:,1]*3600
Dumux_theta_r_002_Evap = data9[200:,3]*3600
Dumux_theta_r_004_Evap = data9[195:,5]*3600 
Dumux_theta_r_006_Evap = data9[194:,7]*3600
Time_Dumux_theta_r_000_Evap = data9[189:,0]/24
Time_Dumux_theta_r_002_Evap = data9[200:,2]/24
Time_Dumux_theta_r_004_Evap = data9[195:,4]/24 
Time_Dumux_theta_r_006_Evap = data9[194:,6]/24 



# Sispat
# Source of values: out1_NMR_10.dat
Sispat_time = data5[:,0]
Sispat_theta_r_000_Oxygen = data5[:,6] 
Sispat_theta_r_002_Oxygen = data6[:,6]
Sispat_theta_r_004_Oxygen = data7[:,6]
Sispat_theta_r_006_Oxygen = data8[:,6]
Sispat_theta_r_000_Deuterium = data5[:,5] 
Sispat_theta_r_002_Deuterium = data6[:,5]
Sispat_theta_r_004_Deuterium = data7[:,5]
Sispat_theta_r_006_Deuterium= data8[:,5]
Sispat_theta_r_000_Sat = data5[:,4]/0.44 
Sispat_theta_r_002_Sat = data6[:,4]/0.44 
Sispat_theta_r_004_Sat = data7[:,4]/0.44 
Sispat_theta_r_006_Sat = data8[:,4]/0.44 

# Source of values: out2_NMR_10.dat
Sispat_time_evaporation = data13[:2365,0]/24
Sispat_theta_r_000_Evap = data13[:2365,14]*3600/2.5e6
Sispat_theta_r_002_Evap = data10[:2365,13]*3600/2.5e6
Sispat_theta_r_004_Evap = data11[:2365,13]*3600/2.5e6 
Sispat_theta_r_006_Evap = data12[:2365,13]*3600/2.5e6 

Sispat_time_evaporation_2 = data13[2366:,0]/24
Sispat_theta_r_000_Evap_2 = data13[2366:,13]*3600/2.5e6
Sispat_theta_r_002_Evap_2 = data10[2366:,12]*3600/2.5e6
Sispat_theta_r_004_Evap_2 = data11[2366:,12]*3600/2.5e6 
Sispat_theta_r_006_Evap_2 = data12[2366:,12]*3600/2.5e6 


fig, ((axs1, axs2, axs3, axs7),( axs4, axs5, axs6, axs8) ) = plt.subplots(2,4, sharex=True, sharey=False, figsize = (15,10))

# Oxygen 
#Sispat
# Initial values for oxygen-18. Sequence starts at day 2. Source of initial values: out2_NMR_10.dat "CL0_5mm"
#axs1.plot([1/24,2], [-7.99,-3.795], linestyle='-', color = 'red',linewidth=2)
#axs1.plot([1/24,2], [-7.99,-3.804], linestyle='-.',color = 'red' ,alpha = 0.4,linewidth=2)
#axs1.plot([1/24,2], [-7.99,-3.811], '--', color = 'orange')
#axs1.plot([1/24,2], [-7.99,-3.816], linestyle='dotted', color = 'orange',alpha = 0.8,linewidth=2)

# Values after day 2
axs1.plot(Sispat_time, Sispat_theta_r_000_Oxygen, linestyle='-', color = 'red',linewidth=2, label=u'$S_{wr}$ = 0')
axs1.plot(Sispat_time, Sispat_theta_r_002_Oxygen, linestyle='-.',color = 'red' ,alpha = 0.4,linewidth=2, label=u'$S_{wr}$ = 0.02')
axs1.plot(Sispat_time, Sispat_theta_r_004_Oxygen,'--', color = 'orange' ,label=u'$S_{wr}$ = 0.04')
axs1.plot(Sispat_time,Sispat_theta_r_006_Oxygen,linestyle='dotted', color = 'orange',alpha = 0.8, linewidth=2, label=u'$S_{wr}$ = 0.06')

axs1.set_ylim(-5,17)

# specifying horizontal line type
axs1.axvline(x = 15, color = 'g', linestyle = '-')
axs1.axvline(x = 111, color = 'g', linestyle = '--')
axs1.axvline(x = 131, color = 'g', linestyle = '-.')

#Dumux
axs4.plot(Dumux_time, Dumux_theta_r_000_Oxygen, linestyle='-', color = 'darkblue',linewidth=2, label=u'$S_{wr} = 0$')
axs4.plot(Dumux_time_002, Dumux_theta_r_002_Oxygen, linestyle='-.',color = 'darkblue' ,alpha = 0.6, linewidth=2, label=u'$S_{wr} = 0.02$')
axs4.plot(Dumux_time_004, Dumux_theta_r_004_Oxygen,'--', color = 'blue' ,label=u'$S_{wr} = 0.04$')
axs4.plot(Dumux_time_006, Dumux_theta_r_006_Oxygen,linestyle='dotted', color = 'blue' ,alpha = 0.6,label=u'$S_{wr} = 0.06$')

axs4.set_ylim(-5,17)

axs4.axvline(x = 15, color = 'g', linestyle = '-')
axs4.axvline(x = 116, color = 'g', linestyle = '--')
axs4.axvline(x = 137, color = 'g', linestyle = '-.')


# Deuterium 
# Values after day 2
axs2.plot(Sispat_time, Sispat_theta_r_000_Deuterium, linestyle='-', color = 'red',linewidth=2, label='')
axs2.plot(Sispat_time, Sispat_theta_r_002_Deuterium, linestyle='-.',color = 'red' ,alpha = 0.6,linewidth=2, label='')
axs2.plot(Sispat_time, Sispat_theta_r_004_Deuterium,'--', color = 'orange' ,label='')
axs2.plot(Sispat_time, Sispat_theta_r_006_Deuterium,linestyle='dotted', color = 'orange' ,alpha = 0.8,label='')

axs2.set_ylim(-40,28)

axs2.axvline(x = 15, color = 'g', linestyle = '-')
axs2.axvline(x = 111, color = 'g', linestyle = '--')
axs2.axvline(x = 131, color = 'g', linestyle = '-.')

axs5.plot(Dumux_time, Dumux_theta_r_000_Deuterium, linestyle='-', color = 'darkblue',linewidth=2, label='')
axs5.plot(Dumux_time_002, Dumux_theta_r_002_Deuterium, linestyle='-.',color = 'darkblue' ,alpha = 0.6,linewidth=2, label='')
axs5.plot(Dumux_time_004, Dumux_theta_r_004_Deuterium,'--', color = 'blue' ,label='')
axs5.plot(Dumux_time_006, Dumux_theta_r_006_Deuterium,linestyle='dotted', color = 'blue',alpha = 0.6, label='')

axs5.set_ylim(-40,28)

axs5.axvline(x = 15, color = 'g', linestyle = '-')
axs5.axvline(x = 116, color = 'g', linestyle = '--')
axs5.axvline(x = 137, color = 'g', linestyle = '-.')


# Saturation 
axs3.plot(Sispat_time, Sispat_theta_r_000_Sat, linestyle='-', color = 'red',linewidth=2, label='')
axs3.plot(Sispat_time, Sispat_theta_r_002_Sat,  linestyle='-.',color = 'red' ,alpha = 0.6,linewidth=2, label='')
axs3.plot(Sispat_time, Sispat_theta_r_004_Sat, '--', color = 'orange' ,label='')
axs3.plot(Sispat_time, Sispat_theta_r_006_Sat, linestyle='dotted', color = 'orange',alpha = 0.8, label='')

axs3.set_ylim(0,0.7)

axs3.axvline(x = 15, color = 'g', linestyle = '-')
axs3.axvline(x = 111, color = 'g', linestyle = '--')
axs3.axvline(x = 131, color = 'g', linestyle = '-.')

axs6.plot(Dumux_time, Dumux_theta_r_000_Sat, linestyle='-', color = 'darkblue',linewidth=2, label='')
axs6.plot(Dumux_time_002, Dumux_theta_r_002_Sat, linestyle='-.',color = 'darkblue', alpha = 0.6, linewidth=2, label='')
axs6.plot(Dumux_time_004, Dumux_theta_r_004_Sat,'--', color = 'blue' ,label='')
axs6.plot(Dumux_time_006, Dumux_theta_r_006_Sat,linestyle='dotted', color = 'blue',alpha = 0.6 ,label='')

axs6.set_ylim(0,0.7)

axs6.axvline(x = 15, color = 'g', linestyle = '-')
axs6.axvline(x = 116, color = 'g', linestyle = '--')
axs6.axvline(x = 137, color = 'g', linestyle = '-.')

#Evaporation 

axs7.plot(Sispat_time_evaporation, Sispat_theta_r_000_Evap, linestyle='-', color = 'red',linewidth=2,  label=u'$S_{wr}$ = 0')
axs7.plot(Sispat_time_evaporation, Sispat_theta_r_002_Evap,  linestyle='-.',color = 'red' ,alpha = 0.6,linewidth=2, label=u'$S_{wr}$ = 0.0455')
axs7.plot(Sispat_time_evaporation, Sispat_theta_r_004_Evap, '--', color = 'orange' ,label=u'$S_{wr}$ = 0.091')
axs7.plot(Sispat_time_evaporation, Sispat_theta_r_006_Evap, linestyle='dotted', color = 'orange',alpha = 0.8, label=u'$S_{wr}$ = 0.1364')


axs7.plot(Sispat_time_evaporation_2, Sispat_theta_r_000_Evap_2, linestyle='-', color = 'red',linewidth=2)
axs7.plot(Sispat_time_evaporation_2, Sispat_theta_r_002_Evap_2,  linestyle='-.',color = 'red' ,alpha = 0.6,linewidth=2)
axs7.plot(Sispat_time_evaporation_2, Sispat_theta_r_004_Evap_2, '--', color = 'orange' )
axs7.plot(Sispat_time_evaporation_2, Sispat_theta_r_006_Evap_2, linestyle='dotted', color = 'orange',alpha = 0.8)

axs7.axvline(x = 15, color = 'g', linestyle = '-')
axs7.axvline(x = 111, color = 'g', linestyle = '--')
axs7.axvline(x = 131, color = 'g', linestyle = '-.')

axs7.set_ylim(0,0.1)

axs8.plot(Time_Dumux_theta_r_000_Evap, Dumux_theta_r_000_Evap, linestyle='-', color = 'darkblue',linewidth=2, label=u'$S_{wr}$ = 0')
axs8.plot(Time_Dumux_theta_r_002_Evap, Dumux_theta_r_002_Evap, linestyle='-.',color = 'darkblue', alpha = 0.6, linewidth=2,label=u'$S_{wr}$ = 0.0455')
axs8.plot(Time_Dumux_theta_r_004_Evap, Dumux_theta_r_004_Evap,'--', color = 'blue' ,label=u'$S_{wr}$ = 0.091')
axs8.plot(Time_Dumux_theta_r_006_Evap, Dumux_theta_r_006_Evap, linestyle='dotted', color = 'blue',alpha = 0.6 ,label=u'$S_{wr}$ = 0.1364')

axs8.axvline(x = 15, color = 'g', linestyle = '-')
axs8.axvline(x = 116, color = 'g', linestyle = '--')
axs8.axvline(x = 137, color = 'g', linestyle = '-.')

axs8.set_ylim(0,0.1)

#axs1.set_title('HDO')


# Set common label for depth
#fig.text(0.02, 0.5, 'Depth [m]', fontsize=font['size'], fontweight=font['weight'], ha='center', va='center', rotation='vertical')

fig.text(0.03, 0.96, "a)", fontsize=font['size'], fontweight=font['weight'], ha='center', va='center', rotation='horizontal')
fig.text(0.3, 0.96, "b)", fontsize=font['size'], fontweight=font['weight'], ha='center', va='center', rotation='horizontal')
fig.text(0.53, 0.96, "c)", fontsize=font['size'], fontweight=font['weight'], ha='center', va='center', rotation='horizontal')
fig.text(0.76, 0.96, "d)", fontsize=font['size'], fontweight=font['weight'], ha='center', va='center', rotation='horizontal')



fig.text(0.52, 0.985,"I) SiSPAT-Isotope", fontsize=font['size'], fontweight=font['weight'], ha='center', va='center', rotation='horizontal')
fig.text(0.52, 0.52, "II) DuMu$^x$", fontsize=font['size'], fontweight=font['weight'], ha='center', va='center', rotation='horizontal')


#set x-labels
#axs1.set_ylabel("Saturation [m$^{3}$m$^{-3}$]", fontsize=font['size'], fontweight=font['weight'])
#axs2.set_xlabel(u'$\delta^{^{18}O}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'])


axs1.set_ylabel(u'$\delta^{^{18}O}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'])
axs2.set_ylabel(u'$\delta^{^2H}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'])
axs3.set_ylabel("$S_l [m^3m^{-3}$]", fontsize=font['size'], fontweight=font['weight'])
axs4.set_ylabel(u'$\delta^{^{18}O}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'])
axs5.set_ylabel(u'$\delta^{^2H}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'])
axs6.set_ylabel("$S_l [m^3m^{-3}$]", fontsize=font['size'], fontweight=font['weight'])
axs7.set_ylabel("Evaporation rate [mm/h]", fontsize=font['size'], fontweight=font['weight'])
axs8.set_ylabel("Evaporation rate [mm/h]", fontsize=font['size'], fontweight=font['weight'])
axs4.set_xlabel("Time [d]", fontsize=font['size'], fontweight=font['weight'])
axs5.set_xlabel("Time [d]", fontsize=font['size'], fontweight=font['weight'])
axs6.set_xlabel("Time [d]", fontsize=font['size'], fontweight=font['weight'])
axs8.set_xlabel("Time [d]", fontsize=font['size'], fontweight=font['weight'])

axs8.set_ylim(0,0.1)

#axs6.legend(loc='upper center', bbox_to_anchor=(0.5, 0.07), frameon=True, ncol=4)
axs7.legend(loc='upper right', frameon=True, ncol=1)
axs8.legend(loc='upper right', frameon=True, ncol=1)

fig.tight_layout(pad=2, w_pad=1.0, h_pad=1.0)

# place a text box in upper left in axes coords
# these are matplotlib.patch.Patch properties
#textstr = 'a)\n'
#ax.text(-1.5, 1.08, textstr, transform=ax.transAxes, fontsize=font['size'], fontweight=font['weight'], verticalalignment='top')

plt.show()
fig.savefig("Isotope_sensitvity_35h.png")
fig.savefig("Isotope_sensitvity_35h.pdf")
