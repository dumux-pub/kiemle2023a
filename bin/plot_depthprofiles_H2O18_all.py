#!/usr/bin/env python
#change to plot from excel table
import glob
import sys
import csv
import numpy as np
import os
import copy
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import json
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

plt.style.use('seaborn-whitegrid')
col = plt.rcParams['axes.prop_cycle']


font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 14}

font2 = {'family' : 'normal',
         'weight' : 'bold',
         'size'   : 14}

plt.rc('font', **font)

# this can be probably solved also with some loops
data_path = '../Experiment/Experiment/atmosphericH2O18.csv'
with open(data_path, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data = np.array(list(reader)).astype(float)
 
 # 0.01 m   
data_path1 = '../Experiment/Dumux/590.csv'
with open(data_path1, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data1 = np.array(list(reader)).astype(float)

data_path2 = '../Experiment/SiSPAT/001m.csv'
with open(data_path2, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data2 = np.array(list(reader)).astype(float)
    
data_path3 = '../Experiment/Experiment/001m.csv'
with open(data_path3, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data3 = np.array(list(reader)).astype(float)   
     
# 0.05 m 
data_path4 = '../Experiment/Dumux/550.csv'
with open(data_path4, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data4 = np.array(list(reader)).astype(float)

data_path5 = '../Experiment/SiSPAT/005m.csv'
with open(data_path5, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data5 = np.array(list(reader)).astype(float)
    
data_path6 = '../Experiment/Experiment/005m.csv'
with open(data_path6, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data6 = np.array(list(reader)).astype(float)
    

#0.1 m 
data_path7 = '../Experiment/Dumux/500.csv'
with open(data_path7, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data7 = np.array(list(reader)).astype(float)
    

data_path8 = '../Experiment/SiSPAT/01m.csv'
with open(data_path8, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data8 = np.array(list(reader)).astype(float)
    
data_path9 = '../Experiment/Experiment/01m.csv'
with open(data_path9, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data9 = np.array(list(reader)).astype(float)


#0.4 m     
data_path10 = '../Experiment/Dumux/200.csv'
with open(data_path10, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data10 = np.array(list(reader)).astype(float)

data_path11 = '../Experiment/SiSPAT/04m.csv'
with open(data_path11, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data11 = np.array(list(reader)).astype(float)
    
data_path12 = '../Experiment/Experiment/04m.csv'
with open(data_path12, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data12 = np.array(list(reader)).astype(float)
    


# Atmosphere
yCoordinate_Atmosphere = data[:,1]
Atmosphere = data[:,0]/3600/24

# 0.01 m 
yCoordinate_Dumux = data1[:,27]
Dumux = data1[:,1]/3600/24

yCoordinate_Sispat = data2[:,2]
Sispat = data2[:,0]

yCoordinate_Experiments = data3[:,6]
Experiments = data3[:,0]

# 0.05 m 
yCoordinate_Dumux_1 = data4[:,27]
Dumux_1 = data4[:,1]/3600/24

yCoordinate_Sispat_1 = data5[:,7]
Sispat_1 = data5[:,0]

yCoordinate_Experiments_1 = data6[:,5]
Experiments_1 = data6[:,0]


#0.1 m 
yCoordinate_Dumux_2 = data7[:,27]
Dumux_2 = data7[:,1]/3600/24

yCoordinate_Sispat_2 = data8[:,7]
Sispat_2 = data8[:,0]

yCoordinate_Experiments_2 = data9[:,5]
Experiments_2 = data9[:,0]


#0.4 m 
yCoordinate_Dumux_3 = data10[:,27]
Dumux_3 = data10[:,1]/3600/24


yCoordinate_Sispat_3 = data11[:,6]
Sispat_3 = data11[:,0]

yCoordinate_Experiments_3 = data12[:,4]
Experiments_3 = data12[:,0]


fig, (axs1, axs2, axs3, axs4, ) = plt.subplots(4,1, sharex=True, sharey=False, figsize = (8,13))
# 0.01 m 

axs00 = axs1.twinx()
axs1.set_zorder(10)
axs1.patch.set_visible(False)
axs1.plot(Dumux, yCoordinate_Dumux, linestyle='-', linewidth=2, label='DuMu$^x$')
axs1.plot(Sispat, yCoordinate_Sispat, linestyle='-.', linewidth=2, label='SiSPAT-Isotope')
axs1.plot(Experiments, yCoordinate_Experiments,'--', color = 'grey' ,label='Experiment (PM)')
axs00.plot(Atmosphere, yCoordinate_Atmosphere,'-', color = 'lightgrey',label='Experiment (FF)')

axs00.set_ylim(-45,-10)
axs1.set_ylim(-15,20)


# 0.05 m 
axs0 = axs2.twinx()
axs2.set_zorder(10)
axs2.patch.set_visible(False)
axs0.plot(Atmosphere, yCoordinate_Atmosphere,'-', color = 'lightgrey')
axs2.plot(Dumux_1, yCoordinate_Dumux_1, linestyle='-', linewidth=2)
axs2.plot(Sispat_1, yCoordinate_Sispat_1, linestyle='-.', linewidth=2)
axs2.plot(Experiments_1, yCoordinate_Experiments_1,'--', color = 'grey' )

axs0.set_ylim(-45,-10)
axs2.set_ylim(-15,20)


#0.1 m 
#axs0 = axs3.twinx()
#axs0.plot(Atmosphere, yCoordinate_Atmosphere,'-', color = 'lightgrey' )
axs3.plot(Dumux_2, yCoordinate_Dumux_2, linestyle='-', linewidth=2)
axs3.plot(Sispat_2, yCoordinate_Sispat_2, linestyle='-.', linewidth=2)
axs3.plot(Experiments_2, yCoordinate_Experiments_2,'--', color = 'grey' )

axs3.set_ylim(-15,20)

#0.4 m 
axs4.plot(Dumux_3, yCoordinate_Dumux_3, linestyle='-', linewidth=2, label='DuMu$^x$')
axs4.plot(Sispat_3, yCoordinate_Sispat_3, linestyle='-.', linewidth=2, label='SiSPAT-Isotope')
axs4.plot(Experiments_3, yCoordinate_Experiments_3,'--', color = 'grey',label='Experiment (PM)' )
axs4.plot([0], [0],'-', color = 'lightgrey', label='Experiment (FF)')


axs4.set_ylim(-15,20)


#axs1.set_title('HDO')


# Set common label for depth
#fig.text(0.02, 0.5, 'Depth [m]', fontsize=font['size'], fontweight=font['weight'], ha='center', va='center', rotation='vertical')

fig.text(0.5, 0.99, 'a) -0.01 m ', fontsize=font['size'], fontweight=font['weight'], ha='center', va='center', rotation='horizontal')
fig.text(0.5, 0.75, 'b) -0.05 m ', fontsize=font['size'], fontweight=font['weight'], ha='center', va='center', rotation='horizontal')
fig.text(0.5, 0.515, 'c) -0.1 m ', fontsize=font['size'], fontweight=font['weight'], ha='center', va='center', rotation='horizontal')
fig.text(0.5, 0.28, 'd) -0.4 m ', fontsize=font['size'], fontweight=font['weight'], ha='center', va='center', rotation='horizontal')
#set x-labels
#axs1.set_ylabel("Saturation [m$^{3}$m$^{-3}$]", fontsize=font['size'], fontweight=font['weight'])
#axs2.set_xlabel(u'$\delta^{^{18}O}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'])
axs00.set_ylabel(u'$\delta^{^{18}O}_g$[\u2030]', fontsize=font['size'], fontweight=font['weight'])
axs0.set_ylabel(u'$\delta^{^{18}O}_g$[\u2030]', fontsize=font['size'], fontweight=font['weight'])
axs1.set_ylabel(u'$\delta^{^{18}O}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'])
axs2.set_ylabel(u'$\delta^{^{18}O}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'])
axs3.set_ylabel(u'$\delta^{^{18}O}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'])
axs4.set_ylabel(u'$\delta^{^{18}O}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'])
axs4.set_xlabel('Time [d]', fontsize=font['size'], fontweight=font['weight'])


axs4.legend(loc='best', frameon=True, ncol=2)
#fig.tight_layout(pad=2, w_pad=1.0, h_pad=1.0)
fig.tight_layout()


# place a text box in upper left in axes coords
# these are matplotlib.patch.Patch properties
#textstr = 'a)\n'
#ax.text(-1.5, 1.08, textstr, transform=ax.transAxes, fontsize=font['size'], fontweight=font['weight'], verticalalignment='top')

plt.show()
fig.savefig("H2O18_isotopeprofile_all.png")
fig.savefig("H2O18_isotopeprofile_all.pdf")
