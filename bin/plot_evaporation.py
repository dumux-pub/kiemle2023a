#!/usr/bin/env python
#change to plot from excel table
import glob
import sys
import csv
import numpy as np
import os
import copy
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import json
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

plt.style.use('seaborn-whitegrid')
col = plt.rcParams['axes.prop_cycle']

# plot the param study data


font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 14}

font2 = {'family' : 'normal',
         'weight' : 'bold',
         'size'   : 14}

plt.rc('font', **font)


# this can be probably solved also with some loops
data_path = '../Experiment/Dumux/evaporation_rate_hourly.csv'
with open(data_path, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data = np.array(list(reader)).astype(float)
    
data_path2 = '../Experiment/SiSPAT/evaporationrate.csv'
with open(data_path2, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data2 = np.array(list(reader)).astype(float)
    
data_path3 = '../Experiment/Experiment/evaporation_rate.csv'
with open(data_path3, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data3 = np.array(list(reader)).astype(float)



yCoordinate_Dumux = data[:,1]
yCoordinate_Dumux_Cum = data[:,2]
Dumux = data[:,0]/24

yCoordinate_Sispat = data2[:,1]
yCoordinate_Sispat_Cum = data2[:,2]
Sispat = data2[:,0]/24

yCoordinate_Experiments = data3[:,1]
yCoordinate_Experiments_Cum = data3[:,2]
Experiments = data3[:,0]/24




fig, (axs1, axs2) = plt.subplots(1,2, sharex=False, sharey=False, figsize=(10,6))

axs1.plot(Experiments, yCoordinate_Experiments,'--', color = 'grey', alpha=0.5 ,label='Experiment')
axs1.plot(Dumux, yCoordinate_Dumux, linestyle='-', linewidth=2, label='DuMu$^x$')
axs1.plot(Sispat, yCoordinate_Sispat, linestyle='-.', linewidth=2, label='SiSPAT-Isotope')

axs2.plot(Experiments, yCoordinate_Experiments_Cum,'--', color = 'grey', alpha=0.5 ,label='Experiment')
axs2.plot(Dumux, yCoordinate_Dumux_Cum, linestyle='-', linewidth=2, label='DuMu$^x$')
axs2.plot(Sispat, yCoordinate_Sispat_Cum, linestyle='-.', linewidth=2, label='SiSPAT-Isotope')

#axs1.set_ylim([-0.018, 0.0])


#set x-labels
axs1.set_ylabel('Evaporation rate [mm/h]', fontsize=font['size'], fontweight=font['weight'])
axs1.set_xlabel('Time [d]', fontsize=font['size'], fontweight=font['weight'])

axs2.set_ylabel('Cummulative evaporation rate [mm]', fontsize=font['size'], fontweight=font['weight'])
axs2.set_xlabel('Time [d]', fontsize=font['size'], fontweight=font['weight'])

axs2.legend(loc='lower right', frameon= True)
fig.tight_layout(pad=2, w_pad=1.0, h_pad=1.0)


# place a text box in upper left in axes coords
# these are matplotlib.patch.Patch properties
textstr = 'a)\n'
axs1.text(0.01, 1.0, textstr, transform=axs1.transAxes, fontsize=font['size'], fontweight=font['weight'], verticalalignment='top')
textstr = 'b)\n'
axs2.text(1.23, 1.0, textstr, transform=axs1.transAxes, fontsize=font['size'], fontweight=font['weight'], verticalalignment='top')

plt.show()
fig.savefig("evaporationrate.png")
fig.savefig("evaporationrate.pdf")
