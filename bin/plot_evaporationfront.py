#!/usr/bin/env python
#change to plot from excel table
import glob
import sys
import csv
import numpy as np
import os
import copy
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import json
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

plt.style.use('seaborn-whitegrid')
col = plt.rcParams['axes.prop_cycle']


font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 14}

font2 = {'family' : 'normal',
         'weight' : 'bold',
         'size'   : 14}

plt.rc('font', **font)

# this can be probably solved also with some loops
 
 # 0.01 m   
# Open the file for reading
with open('/home/workspace10/Projects/Isotopes/1DModel/Evaluation/Results/Experiment/Dumux/new/evaporationfront_new.csv', 'r') as file:
    reader = csv.reader(file)
    headers = next(reader)
    
    # Initialize an empty list to store the data
    data1 = []

    # Iterate over each row in the CSV file
    for row in reader:
        # Iterate over each element in the row
        row_data = []
        for element in row:
            # Check if the element is not empty and can be converted to float
            if element.strip():  # This checks if the string is not empty after removing leading/trailing whitespace
                row_data.append(float(element))
            else:
                # Handle the empty string case (e.g., by skipping it or assigning a default value)
                row_data.append(0.0)  # Assigning 0.0 as a default value for empty strings
        data1.append(row_data)

# Convert the data list to a NumPy array
data1 = np.array(data1)
    
    
with open('/home/workspace10/Projects/Isotopes/1DModel/Evaluation/Results/Experiment/Dumux/new/evaporationfront_detailed.csv', 'r') as file:
    reader = csv.reader(file)
    headers = next(reader)
    
    # Initialize an empty list to store the data
    data2 = []

    # Iterate over each row in the CSV file
    for row in reader:
        # Iterate over each element in the row
        row_data = []
        for element in row:
            # Check if the element is not empty and can be converted to float
            if element.strip():  # This checks if the string is not empty after removing leading/trailing whitespace
                row_data.append(float(element))
            else:
                # Handle the empty string case (e.g., by skipping it or assigning a default value)
                row_data.append(0.0)  # Assigning 0.0 as a default value for empty strings
        data2.append(row_data)

# Convert the data list to a NumPy array
data2 = np.array(data2)    
    
data_path3 = '../Experiment/Dumux/new/evaporationfront_concentration_3.csv'
with open(data_path3, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data3 = np.array(list(reader)).astype(float)    


Dumux_Time = data1[:,0]/3600/24
GradHDO_Dumux = data1[:,12]
GradHO18_Dumux = data1[:,11]
EF_HDO_Dumux = data1[:,27]
EF_HO18_Dumux = data1[:,26]

Dumux_Time_Conc = data3[:,0]/3600/24
EF_HDO_Dumux_Conc = data3[:,19]
EF_HO18_Dumux_Conc = data3[:,18]

Dumux_Time_EXACT = data2[25:,0]/24
EF_EXACT_HDO_Dumux = -0.6+data2[25:,3]
EF_EXACT_H2O18_Dumux = -0.6+data2[25:,4]
EF_EXACT_maxHDO_Dumux = -0.6+data2[25:,7]
EF_EXACT_maxH2O18_Dumux = -0.6+data2[25:,8]



fig, (axs1, axs2 ) = plt.subplots(2,1, sharex=True, sharey=False, figsize = (9,4))


axs1.plot(Dumux_Time, EF_HDO_Dumux, linestyle='-', linewidth=2, color = 'blue',  label = 'Max. gradient')
axs1.plot(Dumux_Time_Conc, EF_HDO_Dumux_Conc, linestyle='-', color='cyan', alpha=0.6, linewidth=2, label = 'Max. concentration')
axs1.plot(Dumux_Time_EXACT, EF_EXACT_HDO_Dumux, linestyle='-',color='darkblue',  label ='Refined resolution') 
#axs1.plot(Dumux_Time_EXACT, EF_EXACT_maxHDO_Dumux, linestyle='-', color = 'cyan', label = u'$\delta_{i,max}$')  

axs2.plot(Dumux_Time, EF_HO18_Dumux, linestyle='-', linewidth=2,color = 'blue' )
axs2.plot(Dumux_Time_Conc, EF_HO18_Dumux_Conc, linestyle='-', color='cyan', alpha=0.6,linewidth=2, )
axs2.plot(Dumux_Time_EXACT, EF_EXACT_H2O18_Dumux, linestyle='-', color='darkblue' ) 
#axs2.plot(Dumux_Time_EXACT, EF_EXACT_maxH2O18_Dumux, linestyle='-', color = 'cyan', label = u'$\delta_{i,max}$') 


#axs1.set_title('HDO')


# Set common label for depth
#fig.text(0.02, 0.5, 'Depth [m]', fontsize=font['size'], fontweight=font['weight'], ha='center', va='center', rotation='vertical')

fig.text(0.22, 0.94, u'a) $ \delta^{^2H}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'], ha='center', va='center', rotation='horizontal')
fig.text(0.22, 0.54, u'b) $ \delta^{^{18}O}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'], ha='center', va='center', rotation='horizontal')




axs1.set_ylabel(u'Depth [m]', fontsize=font['size'], fontweight=font['weight'])
axs2.set_ylabel(u'Depth [m]', fontsize=font['size'], fontweight=font['weight'])

axs2.set_xlabel(u'Time [d]', fontsize=font['size'], fontweight=font['weight'])




fig.legend(loc='upper center', bbox_to_anchor=(0.5, 0.12), frameon=True, ncol=3)

fig.tight_layout(pad=2, w_pad=1.0, h_pad=2)


#axs1.legend(loc ='best')
#axs2.legend(loc ='best')
#axs2.legend(loc ='best')

# place a text box in upper left in axes coords
# these are matplotlib.patch.Patch properties
#textstr = 'a)\n'
#ax.text(-1.5, 1.08, textstr, transform=ax.transAxes, fontsize=font['size'], fontweight=font['weight'], verticalalignment='top')

plt.show()
fig.savefig("Evaporationfront_Methode_3.png")
fig.savefig("Evaporationfront_Methode_3.pdf")
