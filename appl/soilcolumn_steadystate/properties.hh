// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesNCTests
 * \brief The properties of the soil column isotope problem
 */
#ifndef DUMUX_TWOPNC_SOILCOLUMN_PROPERTIES_HH
#define DUMUX_TWOPNC_SOILCOLUMN_PROPERTIES_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/cctpfa.hh>
#include <dumux/porousmediumflow/2pnc/model.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/material/fluidmatrixinteractions/diffusivityconstanttortuosity.hh>

#include <dumux/volumevariables/volumevariables.hh> //VolVars with alpha
#include <dumux/iofields.hh>
#include <dumux/material/fluidsystems/H2OAirIsotopes.hh>

#include "problem.hh"
#include "spatialparams.hh"



namespace Dumux::Properties {

    // Create new type tags
    namespace TTag {
        struct TwoPNCSoilColumn { using InheritsFrom = std::tuple<TwoPNC, CCTpfaModel>; };
    } // end namespace TTag

    // Set the grid type
    template<class TypeTag>
    struct Grid<TypeTag, TTag::TwoPNCSoilColumn> {using type = Dune::YaspGrid<1>; };

    // Set the problem property
    template<class TypeTag>
    struct Problem<TypeTag, TTag::TwoPNCSoilColumn> { using type = TwoPNCSoilColumnProblem<TypeTag>; };

    // Set fluid configuration
    template<class TypeTag>
    struct FluidSystem<TypeTag, TTag::TwoPNCSoilColumn> {
        using type = FluidSystems::H2OAirIsotopes<GetPropType<TypeTag, Properties::Scalar>>;
    };

    // Set the spatial parameters
    template<class TypeTag>
    struct SpatialParams<TypeTag, TTag::TwoPNCSoilColumn>
    {
        using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
        using Scalar = GetPropType<TypeTag, Properties::Scalar>;
        using type = TwoPNCSoilColumnSpatialParams<GridGeometry, Scalar>;
    };

    // Define whether mole(true) or mass (false) fractions are used
    template<class TypeTag>
    struct UseMoles<TypeTag, TTag::TwoPNCSoilColumn> { static constexpr bool value = true; };

    //! Set the default formulation to pw-Sn: This can be over written in the problem.
    template<class TypeTag>
    struct Formulation<TypeTag, TTag::TwoPNCSoilColumn> { static constexpr auto value = TwoPFormulation::p1s0; };

    //! Set the volume variables property
    template<class TypeTag>
    struct VolumeVariables<TypeTag, TTag::TwoPNCSoilColumn>
    {
    private:
        using PV = GetPropType<TypeTag, Properties::PrimaryVariables>;
        using FSY = GetPropType<TypeTag, Properties::FluidSystem>;
        using FST = GetPropType<TypeTag, Properties::FluidState>;
        using SSY = GetPropType<TypeTag, Properties::SolidSystem>;
        using SST = GetPropType<TypeTag, Properties::SolidState>;
        using PT = typename GetPropType<TypeTag, Properties::SpatialParams>::PermeabilityType;
        using MT = GetPropType<TypeTag, Properties::ModelTraits>;
        using DM = typename GetPropType<TypeTag, Properties::GridGeometry>::DiscretizationMethod;
        static constexpr bool enableIS = getPropValue<TypeTag, Properties::EnableBoxInterfaceSolver>();
        // class used for scv-wise reconstruction of non-wetting phase saturations
        using SR = TwoPScvSaturationReconstruction<DM, enableIS>;
        using BaseTraits = TwoPVolumeVariablesTraits<PV, FSY, FST, SSY, SST, PT, MT, SR>;

        using DT = GetPropType<TypeTag, Properties::MolecularDiffusionType>;
        using EDM = GetPropType<TypeTag, Properties::EffectiveDiffusivityModel>;
        template<class BaseTraits, class DT, class EDM>
        struct NCTraits : public BaseTraits
        {
            using DiffusionType = DT;
            using EffectiveDiffusivityModel = EDM;
        };

    public:
        using type = TwoPIsotopeVolumeVariables<NCTraits<BaseTraits, DT, EDM>>; //VolVars mit alpha
    };

    template<class TypeTag>
    struct IOFields<TypeTag, TTag::TwoPNCSoilColumn> { using type = TwoPNCIsotopeIOFields; };

    //we set the initial and boundary mole fractions for the second == gas phase
    template<class TypeTag>
    struct SetMoleFractionsForFirstPhase<TypeTag, TTag::TwoPNCSoilColumn> { static constexpr bool value = false; };
    template<class TypeTag>
    struct EffectiveDiffusivityModel<TypeTag, TTag::TwoPNCSoilColumn>
    {
        using Scalar = GetPropType<TypeTag, Properties::Scalar>;
        using type = DiffusivityConstantTortuosity<Scalar>;
    };

} // end namespace Dumux::Properties

#endif
