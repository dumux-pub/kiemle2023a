// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Problem for a isothermal steadystate evaporation and isotopic fractionation problem
 */

#ifndef DUMUX_TWOPNC_STEADYSTATE_PROBLEM_HH
#define DUMUX_TWOPNC_STEADYSTATE_PROBLEM_HH


#include <dumux/discretization/cctpfa.hh>
#include <dumux/porousmediumflow/2pnc/model.hh>
#include <dumux/porousmediumflow/problem.hh>

#include <dumux/io/readwritedatafile.hh>
#include <dumux/io/gnuplotinterface.hh>


namespace Dumux {

    template <class TypeTag>
    class TwoPNCSoilColumnProblem : public PorousMediumFlowProblem<TypeTag>
    {
        using ParentType = PorousMediumFlowProblem<TypeTag>;
        using Scalar = GetPropType<TypeTag, Properties::Scalar>;
        using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
        using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
        using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
        using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
        using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
        using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
        using GridView = typename GridGeometry::GridView;

        enum
        {
            // Grid and world dimension
            dim = GridView::dimension,
            dimWorld = GridView::dimensionworld
        };

        using Element = typename GridView::template Codim<0>::Entity;
        using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
        using FVElementGeometry = typename GridGeometry::LocalView;
        using GridVolumeVariables = GetPropType<TypeTag, Properties::GridVolumeVariables>;
        using ElementVolumeVariables = typename GridVolumeVariables::LocalView;
        using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
        using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
        using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
        using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;


        static constexpr bool useMoles = getPropValue<TypeTag, Properties::UseMoles>();
        //  using Radiation = typename Dumux::Radiation<Scalar, GridGeometry>;
        using H2O = Components::TabulatedComponent<Components::H2O<Scalar> >;
        using Air = Dumux::Components::Air<Scalar>;
        using HDO = Dumux::Components::HDO<Scalar>;
        using H2O18 = Dumux::Components::H2O18<Scalar>;

    public:
        TwoPNCSoilColumnProblem(std::shared_ptr<const GridGeometry> GridGeometry)
        : ParentType(GridGeometry)
        {
            // initialize the tables of the fluid system
            FluidSystem::init();

            outputname_ = getParam<std::string>("Problem.Name");
            name_ ="2pnc_soilcolumn_" + outputname_;

            // stating in the console whether mole or mass fractions are used
            if(useMoles)
            {
                std::cout<<"problem uses mole-fractions"<<std::endl;
            }
            else
            {
                std::cout<<"problem uses mass-fractions"<<std::endl;
            }

            //soil input
            pressure_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Pressure");
            saturation_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Saturation");
            deltaD_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.DeltaD");
            deltaO18_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.DeltaO18");

            //atmospheric input
            airTemperature_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.AirTemperature");
            relativeHumidity_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.RelativeHumidity");
            atmosphericHDODataFilePM_ =getParam<std::string>("Problem.AtmosphericHDODataFile");
            readData(atmosphericHDODataFilePM_, atmosphericHDODataPM_);
            atmosphericH2O18DataFilePM_ =getParam<std::string>("Problem.AtmosphericH2O18DataFile");
            readData(atmosphericH2O18DataFilePM_, atmosphericH2O18DataPM_);
        }

        //! Called after every time step
        //! Output the total global exchange term
        template<class GridVariables, class SolutionVector>
        void postTimeStep(const SolutionVector& curSol,
                          const GridVariables& gridVariables)
        {
            Scalar evaporation = 0.0;
            Scalar evaporationHDO = 0.0;
            Scalar evaporationH2O18 = 0.0;


            if (!(time() < 0.0))
            {
                for (const auto& element : elements(this->gridGeometry().gridView()))
                {
                    auto fvGeometry = localView(this->gridGeometry());
                    auto elemVolVars = localView(gridVariables.curGridVolVars());
                    auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());
                    fvGeometry.bind(element);
                    elemVolVars.bind(element, fvGeometry, curSol);
                    elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

                    for (auto&& scvf : scvfs(fvGeometry)){
                        if (scvf.boundary())
                        {
                            auto fluxes = neumann(element, fvGeometry, elemVolVars, elemFluxVarsCache, scvf);

                            evaporationHDO += fluxes[Indices::conti0EqIdx+2]
                            * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor()* FluidSystem::molarMass(FluidSystem::HDOIdx);
                            evaporationH2O18 += fluxes[Indices::conti0EqIdx+3]* scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor()* FluidSystem::molarMass(FluidSystem::H2O18Idx);
                            evaporation += fluxes[Indices::conti0EqIdx]
                            * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor() * FluidSystem::molarMass(FluidSystem::H2OIdx);
                        }

                    }
                }
            }

            std::cout << "Soil evaporation rate: " << evaporation << " kg/s." << '\n';
            std::cout << "Soil evaporation rate: " << evaporationHDO << " kg/s." << '\n';
            std::cout << "Soil evaporation rate: " << evaporationH2O18 << " kg/s." << '\n';

            //do a gnuplot
            x_.push_back(time()/3600); // in hours
            y_.push_back(evaporation);
            y2_.push_back(evaporationHDO);
            y3_.push_back(evaporationH2O18);

            gnuplot_.resetPlot();
            gnuplot_.setXRange(0,std::max(time()/3600, 1.0));
            gnuplot_.setYRange(0.0, 5e-5);
            gnuplot_.setXlabel("time [h]");
            gnuplot_.setYlabel("kg/s");

            gnuplot_.addDataSetToPlot(x_, y_, "evaporation_rate_" + outputname_);
            gnuplot_.addDataSetToPlot(x_, y2_, "evaporation_rate_HDO_" + outputname_);
            gnuplot_.addDataSetToPlot(x_, y3_, "evaporation_rate_H2O18_" + outputname_);
            gnuplot_.plot("evaporation");

        }

        void setTime(Scalar time)
        { time_ = time; }

        const Scalar time() const
        {return time_; }

        void setPreviousTime(Scalar time)
        { previousTime_ = time; }

        const Scalar previousTime() const
        {return previousTime_; }


        /*!
         * \name Problem parameters
         */
        // \{

        /*!
         * \brief Returns the problem name
         * This is used as a prefix for files generated by the simulation.
         */
        const std::string& name() const
        { return name_; }

        //Set temperature constant or by input file
        Scalar refTemperature() const
        { return  airTemperature_;}

        //! \brief Returns the mole fraction of ordinary water in the atmosphere
        //Set the relative humidity constant or by input
        Scalar refMoleFracVapor() const
        { return relativeHumidity_*H2O::vaporPressure(refTemperature())/1e5;}

        //! \brief Returns the mole fraction of isotopic water (HDO) in the atmosphere
        //Set the isotopic composition constant or by input
        Scalar refMoleFracHDOVapor() const
        { return ((evaluateData(atmosphericHDODataPM_, previousTime(), time()))*ref_D_/1000+ref_D_)*refMoleFracVapor();}

        //! \brief Returns the mole fraction of isotopic water (H2O18) in the atmosphere
        //Set the isotopic composition constant or by input
        Scalar refMoleFracH2O18Vapor() const
        { return ((evaluateData(atmosphericH2O18DataPM_, previousTime(), time()))*ref_O18_/1000+ref_O18_)*refMoleFracVapor();}

        //get mole fraction of air in atmosphere
        Scalar refMoleFracAirVapor() const
        { return 1.0 - refMoleFracVapor(); }

        //get moleFraction of H2O in air
        Scalar refMoleFracH2OVapor() const
        { return refMoleFracVapor()-refMoleFracH2O18Vapor()-refMoleFracHDOVapor();}

        //get average molar mass in atmosphere in vapour phase
        Scalar refaverageMolarMass() const
        { return FluidSystem::molarMass(FluidSystem::H2OIdx)*refMoleFracH2OVapor() + FluidSystem::molarMass(FluidSystem::AirIdx)*refMoleFracAirVapor() +FluidSystem::molarMass(FluidSystem::HDOIdx)*refMoleFracHDOVapor() + FluidSystem::molarMass(FluidSystem::H2O18Idx)*refMoleFracH2O18Vapor();}

        //get massFraction of H2O in air
        Scalar refMassFracVapor() const
        { return refMoleFracH2OVapor()* FluidSystem::molarMass(FluidSystem::H2OIdx)/refaverageMolarMass();}

        //get equilibrium fractionation factors (this is needed for phase transition)
        Scalar alpha_GG_HDO(const GlobalPosition &globalPos) const
        { return  exp(-((24844/(this->spatialParams().temperatureAtPos(globalPos)*this->spatialParams().temperatureAtPos(globalPos)))+(-76.248/this->spatialParams().temperatureAtPos(globalPos))+0.052612));}

        Scalar alpha_GG_H2O18(const GlobalPosition &globalPos) const
        { return  exp(-((1137/(this->spatialParams().temperatureAtPos(globalPos)*this->spatialParams().temperatureAtPos(globalPos)))+(-0.4156/this->spatialParams().temperatureAtPos(globalPos))-0.0020667));}


        /*!
         * \name Boundary conditions
         */
        // \{

        /*!
         * \brief Specifies which kind of boundary condition should be
         *        used for which equation on a given boundary segment
         * \param globalPos The global position
         */
        BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
        {
            BoundaryTypes bcTypes;

            bcTypes.setAllNeumann();

            if(globalPos[0] < this->gridGeometry().bBoxMin()[0]+ eps_ )
                bcTypes.setAllDirichlet();

            return bcTypes;
        }

        /*!
         * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
         * Primary variables for implementing a Dirichlet boundary condition at the bottom of the soil column
         * Based on the experiments a Neumann no-flow condtion is sufficient
         * \param globalPos The global position
         *
         */
        PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
        {
            PrimaryVariables priVars;
            priVars.setState(Indices::bothPhases);
            priVars[Indices::switchIdx] = 1.0;

            Scalar gasPressure =  1e5- FluidSystem::H2O::liquidDensity(this->spatialParams().temperatureAtPos(globalPos), 1.0e5)*this->spatialParams().gravity(globalPos)[dimWorld-1]*1.0;
            priVars[Indices::pressureIdx] = gasPressure;

            Scalar R_D_L = deltaD_*ref_D_/1000+ref_D_;
            Scalar R_O18_L = deltaO18_*ref_O18_/1000+ref_O18_;
            Scalar R_D = R_D_L*alpha_GG_HDO(globalPos);
            Scalar R_O18 = R_O18_L*alpha_GG_H2O18(globalPos);

            Scalar vaporPressure = H2O::vaporPressure(this->spatialParams().temperatureAtPos(globalPos));
            priVars[Indices::conti0EqIdx+2] = R_D*vaporPressure/gasPressure;
            priVars[Indices::conti0EqIdx+3] = R_O18*vaporPressure/gasPressure;
            return priVars;
        }

        /*!
         * \brief Evaluates the boundary conditions for a Neumann boundary segment.
         *
         * For this method, the \a priVars parameter stores the mass flux
         * in normal direction of each component. Negative values mean influx.
         * \param globalPos The global position
         *
         * \note The units must be according to either using mole or mass fractions (mole/(m^2*s) or kg/(m^2*s)).
         */
        NumEqVector neumann(const Element& element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const ElementFluxVariablesCache& elemFluxVarsCache,
                            const SubControlVolumeFace& scvf) const
                            {
                                NumEqVector values(0.0);
                                const auto globalPos = scvf.ipGlobal();
                                const auto& volVars = elemVolVars[scvf.insideScvIdx()];

                                if(globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_) // upper Neumann boundary condition
                                {
                                    //get evaporation rate
                                    Scalar evaporation = 1.003e-5;
                                    Scalar evaporationRateMole = (evaporation/ FluidSystem::molarMass(FluidSystem::H2OIdx));

                                    //Boundary layer calculation using evaporation
                                    Scalar BoundaryLayerThickness = volVars.density(FluidSystem::gasPhaseIdx)*volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx, FluidSystem::AirIdx)*(volVars.massFraction(FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx)-refMassFracVapor())/evaporation;
                                    //get resistance factor
                                    Scalar rah = BoundaryLayerThickness/volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx);

                                    //get kinetic fractionation factor
                                    Scalar alpha_k_HDO = volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx)/volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::gasPhaseIdx, FluidSystem::HDOIdx);
                                    Scalar alpha_k_H2O18 = volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx)/volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::gasPhaseIdx, FluidSystem::H2O18Idx);

                                    // Isotope boundary condition
                                    Scalar evaporationHDORateMole = volVars.molarDensity(FluidSystem::gasPhaseIdx)*(volVars.moleFraction(FluidSystem::gasPhaseIdx, FluidSystem::HDOIdx) - refMoleFracHDOVapor())/rah/alpha_k_HDO; //For HDO Flux

                                    Scalar evaporationH2O18RateMole = volVars.molarDensity(FluidSystem::gasPhaseIdx)*(volVars.moleFraction(FluidSystem::gasPhaseIdx, FluidSystem::H2O18Idx) - refMoleFracH2O18Vapor())/rah/alpha_k_H2O18; //For H2O18 Flux

                                    values[Indices::conti0EqIdx] = evaporationRateMole;
                                    values[Indices::conti0EqIdx+1] = -evaporationRateMole;
                                    values[Indices::conti0EqIdx+2] = evaporationHDORateMole;
                                    values[Indices::conti0EqIdx+1] += -evaporationHDORateMole;
                                    values[Indices::conti0EqIdx+3] = evaporationH2O18RateMole;
                                    values[Indices::conti0EqIdx+1] += -evaporationH2O18RateMole;

                                    if (volVars.pressure(FluidSystem::gasPhaseIdx) - 1e5 > 0)
                                    {
                                        values[Indices::conti0EqIdx+1] = (volVars.pressure(FluidSystem::gasPhaseIdx) - 1e5)
                                        /(globalPos - fvGeometry.scv(scvf.insideScvIdx()).center()).two_norm()
                                        *volVars.mobility(FluidSystem::gasPhaseIdx)
                                        *this->spatialParams().permeabilityAtPos(globalPos)
                                        *volVars.molarDensity(FluidSystem::gasPhaseIdx) * volVars.moleFraction(FluidSystem::gasPhaseIdx, FluidSystem::AirIdx);

                                    }
                                    else
                                    {
                                        values[Indices::conti0EqIdx+1] = (volVars.pressure(FluidSystem::gasPhaseIdx) - 1e5)
                                        /(globalPos - fvGeometry.scv(scvf.insideScvIdx()).center()).two_norm()
                                        *volVars.mobility(FluidSystem::gasPhaseIdx)
                                        *this->spatialParams().permeabilityAtPos(globalPos)
                                        *volVars.molarDensity(FluidSystem::gasPhaseIdx) * refMoleFracAirVapor();
                                    }
                                }

                                return values;
                            }


        // \}

        /*!
            * \name Volume terms
            */
        // \{

        /*!
            * \brief Evaluates the initial values for a control volume.
            *
            * \param globalPos The global position
            */
            PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
            { return initial_(globalPos);}

        // \}

    private:
        /*!
         * \brief Evaluates the initial values for a control volume.
         *
         * The internal method for the initial condition
         *
         * \param globalPos The global position
         */
        PrimaryVariables initial_(const GlobalPosition &globalPos) const
        {
            PrimaryVariables priVars(0.0);
            priVars.setState(Indices::bothPhases);
            priVars[Indices::pressureIdx] = 1.0e5;

            //Set primary variables constant over soil column depth
            Scalar R_D_L = deltaD_*ref_D_/1000+ref_D_;
            Scalar R_O18_L = deltaO18_*ref_O18_/1000+ref_O18_;
            Scalar R_D = R_D_L*alpha_GG_HDO(globalPos);
            Scalar R_O18 = R_O18_L*alpha_GG_H2O18(globalPos);
            Scalar vaporPressure = H2O::vaporPressure(this->spatialParams().temperatureAtPos(globalPos));
            priVars[Indices::conti0EqIdx+2] = R_D*vaporPressure/1e5;
            priVars[Indices::conti0EqIdx+3] = R_O18*vaporPressure/1e5;
            priVars[Indices::switchIdx] = saturation_;
            return priVars;
        }
        static constexpr Scalar eps_ = 1e-6;
        std::string name_ ;
        std::string outputname_ ;

        Scalar time_;
        Scalar previousTime_;

        //reference values of the delta notation
        Scalar ref_D_ = 155.76e-6;
        Scalar ref_O18_ = 2005.2e-6;

        Scalar airTemperature_;
        Scalar relativeHumidity_;

        Scalar windSpeed_;
        Scalar pressure_;
        Scalar saturation_;
        Scalar deltaD_;
        Scalar deltaO18_;

        std::string atmosphericHDODataFilePM_;
        std::vector<double> atmosphericHDODataPM_[2];
        std::string atmosphericH2O18DataFilePM_;
        std::vector<double> atmosphericH2O18DataPM_[2];

        Dumux::GnuplotInterface<double> gnuplot_;

        std::vector<double> x_;
        std::vector<double> y_;
        std::vector<double> y2_;
        std::vector<double> y3_;


    };

} // end namespace Dumux
#endif
