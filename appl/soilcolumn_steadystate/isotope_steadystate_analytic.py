from mpl_toolkits import mplot3d 
import numpy as np
import math
import matplotlib.pyplot as plt
from matplotlib import cm



#Set constant parameters 
EvaporationRate       	= 1.003e-5           	# [kgs-1m-2]  constant evaporation rate
Temperature   		= 303              	# [K] constant temperature
RelativeHumidity 	= 0.2              	# [%] constant relative humidity 
InitialHDO          	= 0             	# [per mil] initial and alimentation concentration HDO
AtmosphericHDO 		= -100			# [per mil] constant atmospheric HDO 
InitialH2O18       	= 0              	# [per mil] initial and alimentation concentration H2O18
AtmosphericH2O18       	= -14                  	# [per mil] constant atmospheric H2O18
Tau 			= 0.67			# [-] tortuosity
SaturatedWaterContent	= 0.35			# [-] saturated water content (here: porosity)
Pressure 		= 1.0e5			# [Pa] constant pressure 


# Define HDO_concentration
def f(depth):

	#Calculate liquid diffusion coefficient [m2s-1]
	#HDO-H2O 
	Diff_HDO_H2O = 0.9833*pow(10,-9)*math.exp((-535400/Temperature/Temperature)+(1393.3/Temperature)+2.1876)
	
	#Calculate effective diffusion coefficient [m2s-1]
	Eff_Diff_HDO = Diff_HDO_H2O*Tau*SaturatedWaterContent
	
	#Calculate equilbrium fractionation 
	alpha_HDO = math.exp(-(24844/Temperature/Temperature-76.248/Temperature+0.052612))
	
	#Calculate vapour diffusion coefficient [m2s-1]
	Diff_H2O_gas = 2.17*pow(10,-5)*pow(10,5)/Pressure*pow((Temperature/273.16),1.88)
	Diff_HDO_gas = Diff_H2O_gas/1.0251
	#Get kinetic fractionation factor 
	#only molecular diffusion 
	alpha_kin_HDO = Diff_H2O_gas/Diff_HDO_gas
	
	#TODO: Insert other kinetic fractionation factors
	
	#Get surface concentration 
	HDO_surface = (((1-RelativeHumidity)*alpha_kin_HDO*(1+InitialHDO/1000)+RelativeHumidity*(1+AtmosphericHDO/1000))/alpha_HDO)-1
	#Get concentration [per mil]
	HDO_concentration = (InitialHDO+(HDO_surface-InitialHDO)*math.exp(-EvaporationRate/1000/Eff_Diff_HDO*abs(depth)))*1000

	
	return HDO_concentration
	
# Define H2O18_concentration	
def h(depth):

	#Calculate liquid diffusion coefficient [m2s-1]
	#HDO-H2O 
	Diff_H2O18_H2O = 0.9669*pow(10,-9)*math.exp((-535400/Temperature/Temperature)+(1393.3/Temperature)+2.1876)
	#Calculate effective diffusion coefficient [m2s-1]
	Eff_Diff_H2O18 = Diff_H2O18_H2O*Tau*SaturatedWaterContent
	#Calculate equilbrium fractionation 
	alpha_H2O18 = math.exp(-(1137/Temperature/Temperature-0.4156/Temperature+0.0020667))
	#Calculate vapour diffusion coefficient [m2s-1]
	Diff_H2O_gas = 2.17*pow(10,-5)*pow(10,5)/Pressure*pow((Temperature/273.16),1.88)
	Diff_H2O18_gas = Diff_H2O_gas/1.0285
	
	#Get kinetic fractionation factor 
	#only molecular diffusion 
	alpha_kin_H2O18 = Diff_H2O_gas/Diff_H2O18_gas
	
	#TODO: Insert other kinetic fractionation factors
	
	#Get surface concentration 
	H2O18_surface = (((1-RelativeHumidity)*alpha_kin_H2O18*(1+InitialH2O18/1000)+RelativeHumidity*(1+AtmosphericH2O18/1000))/alpha_H2O18)-1
	#Get concentration [per mil]
	H2O18_concentration = (InitialH2O18+(H2O18_surface-InitialH2O18)*math.exp(-EvaporationRate/1000/Eff_Diff_H2O18*abs(depth)))*1000
	
	return H2O18_concentration

F = np.vectorize(f) 	
H = np.vectorize(h) 	
	
#Plot 
depth = np.linspace(0.0, -1.0,1000)

plt.plot( F(depth), depth, color='red', label = 'HDO' )
plt.plot( H(depth), depth, color='blue', label = 'H2O18')

plt.xlabel('isotopic composition [per mil]')
plt.ylabel('depth [m]')

ax = plt.gca()

ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)


plt.show()
