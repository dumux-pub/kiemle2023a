dune_add_test(NAME test_2pnc_soilcolumn_isotope_output
              SOURCES main.cc)
dune_symlink_to_source_files(FILES params.input)
              
dune_add_test(NAME test_2pncni_soilcolumn_isotope_output
              SOURCES main.cc
              COMPILE_DEFINITIONS NONISOTHERMAL=1)
dune_symlink_to_source_files(FILES params_nonisothermal.input)
#set(CMAKE_BUILD_TYPE Debug)


