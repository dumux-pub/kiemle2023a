// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Problem where isotopic water and ordinary water evaporates from a soil column.
 */

#ifndef DUMUX_TWOPNC_SOILCOLUMN_PROBLEM_HH
#define DUMUX_TWOPNC_SOILCOLUMN_PROBLEM_HH


#include <dumux/discretization/cctpfa.hh>
#include <dumux/porousmediumflow/2pnc/model.hh>
#include <dumux/porousmediumflow/problem.hh>

#include <dumux/io/readwritedatafile.hh>
#include <dumux/io/gnuplotinterface.hh>


namespace Dumux {

    template <class TypeTag>
    class TwoPNCSoilColumnProblem : public PorousMediumFlowProblem<TypeTag>
    {
        using ParentType = PorousMediumFlowProblem<TypeTag>;
        using Scalar = GetPropType<TypeTag, Properties::Scalar>;
        using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
        using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
        using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
        using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
        using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
        using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
        using GridView = typename GridGeometry::GridView;

        enum
        {
            // Grid and world dimension
            dim = GridView::dimension,
            dimWorld = GridView::dimensionworld
        };

        using Element = typename GridView::template Codim<0>::Entity;
        using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
        using FVElementGeometry = typename GridGeometry::LocalView;
        using GridVolumeVariables = GetPropType<TypeTag, Properties::GridVolumeVariables>;
        using ElementVolumeVariables = typename GridVolumeVariables::LocalView;
        using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
        using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
        using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
        using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;


        static constexpr bool useMoles = getPropValue<TypeTag, Properties::UseMoles>();
        //  using Radiation = typename Dumux::Radiation<Scalar, GridGeometry>;
        using H2O = Components::TabulatedComponent<Components::H2O<Scalar> >;
        using Air = Dumux::Components::Air<Scalar>;
        using HDO = Dumux::Components::HDO<Scalar>;
        using H2O18 = Dumux::Components::H2O18<Scalar>;

    public:
        TwoPNCSoilColumnProblem(std::shared_ptr<const GridGeometry> GridGeometry)
        : ParentType(GridGeometry)
        {
            // initialize the tables of the fluid system
            FluidSystem::init();

            outputname_ = getParam<std::string>("Problem.Name");
            #if NONISOTHERMAL
            name_ ="2pncni_soilcolumn_" + outputname_;
            #else
            name_ ="2pnc_soilcolumn_" + outputname_;
            #endif

            // stating in the console whether mole or mass fractions are used
            if(useMoles)
            {
                std::cout<<"problem uses mole-fractions"<<std::endl;
            }
            else
            {
                std::cout<<"problem uses mass-fractions"<<std::endl;
            }

            temperatureDataFilePM_ =getParam<std::string>("Problem.TemperatureDataFile");
            readData(temperatureDataFilePM_, temperatureDataPM_);
            relativeHumidityDataFilePM_ =getParam<std::string>("Problem.RelativeHumidityDataFile");
            readData(relativeHumidityDataFilePM_, relativeHumidityDataPM_);
            radiationDataFilePM_ =getParam<std::string>("Problem.RadiationDataFile");
            readData(radiationDataFilePM_, radiationDataPM_);
            atmosphericHDODataFilePM_ =getParam<std::string>("Problem.AtmosphericHDODataFile");
            readData(atmosphericHDODataFilePM_, atmosphericHDODataPM_);
            atmosphericH2O18DataFilePM_ =getParam<std::string>("Problem.AtmosphericH2O18DataFile");
            readData(atmosphericH2O18DataFilePM_, atmosphericH2O18DataPM_);

            pressure_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Pressure");
            saturation_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Saturation");
            deltaD_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.DeltaD");
            deltaO18_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.DeltaO18");

            height_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Height");
            heightWK_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.HeightWaterKurve");
            heightWT_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.HeightWaterTable");

            airTemperature_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.AirTemperature");
            relativeHumidity_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.RelativeHumidity");
            atmosphericDeltaD_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.AtmosphericDeltaD");
            atmosphericDeltaO18_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.AtmosphericDeltaO18");

            //Set boundary layer constant or calculate boundary layer thickness
            boundaryLayerThickness_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.BoundaryLayerThickness");
            windSpeed_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.WindSpeed");
            distance_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Distance");

            plotOutput_ = getParam<bool>("Output.PlotOutput");
        }

        //! Called after every time step
        //! Output the total global exchange term
        template<class GridVariables, class SolutionVector>
        void postTimeStep(const SolutionVector& curSol,
                          const GridVariables& gridVariables)
        {
            Scalar evaporation = 0.0;
            Scalar evaporationHDO = 0.0;
            Scalar evaporationH2O18 = 0.0;
            Scalar gradientHDO = 0.0;
            Scalar heightHDO = 0.0;
            Scalar gradientHO18 = 0.0;
            Scalar heightHO18 = 0.0;
            #if NONISOTHERMAL
            Scalar gradientTemperature = 0.0;
            Scalar heightTemperature = 0.0;
            #endif
            std::vector<double> gradientcollectorHDO;
            std::vector<double> gradientcollectorHO18;
            #if NONISOTHERMAL
            std::vector<double> gradientcollectorTemperature;
            std::vector<double> heighthelper;
            #endif


            if (!(time() < 0.0))
            {
                for (const auto& element : elements(this->gridGeometry().gridView()))
                {
                    auto fvGeometry = localView(this->gridGeometry());
                    auto elemVolVars = localView(gridVariables.curGridVolVars());
                    auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());
                    fvGeometry.bind(element);
                    elemVolVars.bind(element, fvGeometry, curSol);
                    elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

                    for (auto&& scvf : scvfs(fvGeometry))
                    {
                        if (scvf.boundary())
                        {
                            auto fluxes = neumann(element, fvGeometry, elemVolVars, elemFluxVarsCache, scvf);

                            evaporationHDO += fluxes[Indices::conti0EqIdx+2]
                            * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor()* FluidSystem::molarMass(FluidSystem::HDOIdx);
                            evaporationH2O18 += fluxes[Indices::conti0EqIdx+3]* scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor()* FluidSystem::molarMass(FluidSystem::H2O18Idx);
                            evaporation += fluxes[Indices::conti0EqIdx]
                            * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor() * FluidSystem::molarMass(FluidSystem::H2OIdx);
                        }
                    else
                        {
                            // get inside/outside volume variables
                            const auto& insideVolVars = elemVolVars[scvf.insideScvIdx()];
                            const auto& outsideVolVars = elemVolVars[scvf.outsideScvIdx()];

                            const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
                            const auto& outsideScv = fvGeometry.scv(scvf.outsideScvIdx());

                            auto distance = insideScv.center()-outsideScv.center();
                            //gradient is inside x - outside x / distance
                            #if NONISOTHERMAL
                            Scalar gradienthelperTemperature = (insideVolVars.temperature() - outsideVolVars.temperature())/distance;
                            #endif
                            //gradient is inside x - outside x / distance
                            Scalar gradienthelperHDO = (insideVolVars.massFraction(0,2) - outsideVolVars.massFraction(0,2))/distance;
                            Scalar gradienthelperHO18 = (insideVolVars.massFraction(0,3) - outsideVolVars.massFraction(0,3))/distance;

                            gradientcollectorHDO.push_back(gradienthelperHDO);
                            gradientcollectorHO18.push_back(gradienthelperHO18);
                            #if NONISOTHERMAL
                            gradientcollectorTemperature.push_back(gradienthelperTemperature);
                            #endif
                            heighthelper.push_back(scvf.ipGlobal());

                        }

                    }
                }
            }

            std::cout << "Soil evaporation rate: " << evaporation << " kg/s." << '\n';
            std::cout << "Soil evaporation rate: " << evaporationHDO << " kg/s." << '\n';
            std::cout << "Soil evaporation rate: " << evaporationH2O18 << " kg/s." << '\n';

            //get the maximum of the gradients
            gradientHDO = *std::max_element(gradientcollectorHDO.begin(), gradientcollectorHDO.end());
            gradientHO18 = *std::max_element(gradientcollectorHO18.begin(), gradientcollectorHO18.end());
            #if NONISOTHERMAL
            gradientTemperature = *std::max_element(gradientcollectorTemperature.begin(), gradientcollectorTemperature.end());
            #endif

            std::cout<<"max gradient HDO " << gradientHDO<<std::endl;
            std::cout<<"max gradient HO18 " << gradientHO18<<std::endl;
            #if NONISOTHERMAL
            std::cout<<"max gradient temperature " << gradientTemperature<<std::endl;
            #endif
            //geht the position of the maximum gradient for maximum gradient of both isotopes
            heightHDO = heighthelper[std::distance(gradientcollectorHDO.begin(), std::max_element(gradientcollectorHDO.begin(), gradientcollectorHDO.end()))];
            heightHO18 = heighthelper[std::distance(gradientcollectorHO18.begin(), std::max_element(gradientcollectorHO18.begin(), gradientcollectorHO18.end()))];
            #if NONISOTHERMAL
            heightTemperature = heighthelper[std::distance(gradientcollectorTemperature.begin(), std::max_element(gradientcollectorTemperature.begin(), gradientcollectorTemperature.end()))];
            #endif

            std::cout<<"height HDO "<< heightHDO<<std::endl;
            std::cout<<"height HO18 "<< heightHO18<<std::endl;
            #if NONISOTHERMAL
            std::cout<<"height temperature "<< heightTemperature<<std::endl;
            #endif
        }
            void plotOutput()
            {
            GnuplotInterface<Scalar> gnuplot(plotOutput_);
            gnuplot.setOpenPlotWindow(plotOutput_);
            //do a gnuplot
            x_.push_back(time()/3600); // in hours
            y_.push_back(evaporation);
            y2_.push_back(evaporationHDO);
            y3_.push_back(evaporationH2O18);
            y4_.push_back(gradientHDO);
            y5_.push_back(heightHDO);
            y6_.push_back(gradientHO18);
            y7_.push_back(heightHO18);
            y8_.push_back(gradientTemperature);
            y9_.push_back(heightTemperature);

            gnuplot_.resetPlot();
            gnuplot2_.resetPlot();
            gnuplot3_.resetPlot();
            gnuplot4_.resetPlot();
            gnuplot5_.resetPlot();

            gnuplot_.setXRange(0,std::max(time()/3600, 1.0));
            gnuplot2_.setXRange(0,std::max(time()/3600, 1.0));
            gnuplot3_.setXRange(0,std::max(time()/3600, 1.0));
            gnuplot4_.setXRange(0,std::max(time()/3600, 1.0));
            gnuplot5_.setXRange(0,std::max(time()/3600, 1.0));

            gnuplot_.setYRange(0.0, 3.5e-5); //evaporation rate
            gnuplot2_.setYRange(0.0, 7.0e-1); //height
            gnuplot3_.setYRange(0.0, 2e-3); //gradient HDO
            gnuplot4_.setYRange(0.0, 1e-2); //gradient H2O18
            gnuplot5_.setYRange(-2e-2, 1e-2); //gradient temperature
            gnuplot_.setXlabel("time [h]");
            gnuplot2_.setXlabel("time [h]");
            gnuplot3_.setXlabel("time [h]");
            gnuplot4_.setXlabel("time [h]");
            gnuplot5_.setXlabel("time [h]");
            gnuplot_.setYlabel("kg/s");
            gnuplot2_.setYlabel("m");
            gnuplot3_.setYlabel("kg/s/m");
            gnuplot4_.setYlabel("kg/s/m");
            gnuplot5_.setYlabel("K/m");
            #if NONISOTHERMAL
            gnuplot_.addDataSetToPlot(x_, y_, "evaporation_rate_" + outputname_);
            gnuplot_.addDataSetToPlot(x_, y2_, "evaporation_rate_HDO_" + outputname_);
            gnuplot_.addDataSetToPlot(x_, y3_, "evaporation_rate_H2O18_" + outputname_);
            gnuplot5_.addDataSetToPlot(x_, y8_, "gradientTemperature_" + outputname_);
            gnuplot2_.addDataSetToPlot(x_, y9_, "heightTemperature_" + outputname_);
            #else
            gnuplot_.addDataSetToPlot(x_, y_, "evaporation_rate_iso_" + outputname_);
            gnuplot_.addDataSetToPlot(x_, y2_, "evaporation_rate_iso_HDO_" + outputname_);
            gnuplot_.addDataSetToPlot(x_, y3_, "evaporation_rate_iso_H2O18_" + outputname_);
            #endif
            gnuplot3_.addDataSetToPlot(x_, y4_, "gradientHDO_" + outputname_);
            gnuplot2_.addDataSetToPlot(x_, y5_, "heightHDO_" + outputname_);
            gnuplot4_.addDataSetToPlot(x_, y6_, "gradientHO18_" + outputname_);
            gnuplot2_.addDataSetToPlot(x_, y7_, "heightHO18_" + outputname_);

  //          gnuplot_.plot("evaporation");
//             gnuplot2_.plot("height");
    //        gnuplot3_.plot("gradient HDO");
//  gnuplot4_.plot("gradient H2O18");
            gnuplot5_.plot("gradient temperature");
            }


        void setTime(Scalar time)
        { time_ = time; }

        const Scalar time() const
        {return time_; }

         void setPreviousTime(Scalar time)
         { previousTime_ = time; }

         const Scalar previousTime() const
         {return previousTime_; }

        /*!
         * \name Problem parameters
         */
        // \{

        /*!
         * \brief Returns the problem name
         * This is used as a prefix for files generated by the simulation.
         */

        const std::string& name() const
        { return name_; }


        /*!
         * \name Boundary conditions
         */
        // \{

        /*!
         * \brief Specifies which kind of boundary condition should be
         *        used for which equation on a given boundary segment
         * \param globalPos The global position
         */
        BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
        {
            BoundaryTypes bcTypes;

            bcTypes.setAllNeumann();

            //             if(globalPos[0] < this->gridGeometry().bBoxMin()[0]+ eps_ )
            //             bcTypes.setAllDirichlet();

            return bcTypes;
        }

        /*!
         * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
         *
         * \param globalPos The global position
         *
         */

        //Primary variables for implemnting a Dirichlet boundary condition at the bottom of the soil column
        //Based on the experiments a Neumann no-flow condtion is sufficient
        PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
        {
            PrimaryVariables priVars;
            priVars.setState(Indices::bothPhases);
            priVars[Indices::switchIdx] = 1.0;
            Scalar temperature = this->spatialParams().temperatureAtPos(globalPos);
            #if NONISOTHERMAL
            priVars[Indices::temperatureIdx] = temperature;
            #endif
            Scalar gasPressure =  1e5- FluidSystem::H2O::liquidDensity(303.0, 1.0e5)*this->spatialParams().gravity(globalPos)[dimWorld-1]*1.0;
            priVars[Indices::pressureIdx] = gasPressure;

            Scalar R_D_L = deltaD_*ref_D_/1000+ref_D_;
            Scalar R_O18_L = deltaO18_*ref_O18_/1000+ref_O18_;
            Scalar R_D = R_D_L*(exp(-((24844/(temperature*temperature))+(-76.248/temperature)+0.052612)));
            Scalar R_O18 = R_O18_L*(exp(-((1137/(temperature*temperature))+(-0.4156/temperature)-0.0020667)));

            Scalar vaporPressure = H2O::vaporPressure(temperature);
            priVars[Indices::conti0EqIdx+2] = R_D*vaporPressure/gasPressure;
            priVars[Indices::conti0EqIdx+3] = R_O18*vaporPressure/gasPressure;


            return priVars;
        }


        //Set temperature constant or by input file
        Scalar refTemperature() const{
        //      return 273.15 + evaluateData(temperatureDataPM_, previousTime(), time());}
        return  airTemperature_;}

        //Set boundary layer thickness in input file or calculate bounary layer thickness
        // 1.8 equals the measurement height of the wind speed
        Scalar boundarylayerthickness() const
        //         {
        //             Scalar Re = windSpeed_ * 1.8 * Air::gasDensity(refTemperature(), 1e5)/ Air::gasViscosity(refTemperature(),1e5);
        //             if (Re < 5*1e5) { //laminar flow (Schlichting, 2017; White, 2011)
        //                 using std::sqrt;
        //                 return  5.0*sqrt(Air::gasViscosity(refTemperature(),1e5)*distance_ / (0.99*windSpeed_*Air::gasDensity(refTemperature(),1e5)));
        //             }
        //             else { //turbulent flow (White, 2011)
        //                 using std::pow;
        //                 return  0.16 * distance_ / pow(Re, (1/7));}
        //         }
        {return boundaryLayerThickness_;}

        //! \brief Returns the mole fraction of ordinary water in the atmosphere
        //Set the relative humidity constant or by input
        Scalar refMoleFracVapor() const
//                  {return  evaluateData(relativeHumidityDataPM_, previousTime(), time())*H2O::vaporPressure(refTemperature())/1e5;}
        {return relativeHumidity_*H2O::vaporPressure(refTemperature())/1e5;}

        //! \brief Returns the mole fraction of isotopic water (HDO) in the atmosphere
        //Set the isotopic composition constant or by input
        Scalar refMoleFracHDOVapor() const
        //         {return ((evaluateData(atmosphericHDODataPM_, previousTime(), time()))*ref_D_/1000+ref_D_)*refMoleFracVapor();}
        {return (atmosphericDeltaD_*ref_D_/1000+ref_D_)*refMoleFracVapor();}

        //! \brief Returns the mole fraction of isotopic water (H2O18) in the atmosphere
        //Set the isotopic composition constant or by input
        Scalar refMoleFracH2O18Vapor() const
        //         {return ((evaluateData(atmosphericH2O18DataPM_, previousTime(), time()))*ref_O18_/1000+ref_O18_)*refMoleFracVapor(); }
        {return (atmosphericDeltaO18_*ref_O18_/1000+ref_O18_)*refMoleFracVapor();}

        //get mole fraction of air in atmosphere
        Scalar refMoleFracAirVapor() const
        {return 1.0 - refMoleFracVapor(); }

        //get friction velocity at the surface
        Scalar FrictionVelocity() const
        {return windSpeed_ * 0.4 / log(heightWind_/surfaceRoughness_);}

        // get the kinematic viscosity of air
        Scalar kinematicVisocsity() const
        {return  Air::gasViscosity(refTemperature(), 1e5) /Air::gasDensity(refTemperature(), 1e5);}

         // Caculation of Reynold's number
         Scalar Rex() const
        {return FrictionVelocity()* surfaceRoughness_/ kinematicVisocsity();}

        // Calculate exponent nk for kinetic fractionation factor after Merlivat/Jouzel(1978)
        Scalar nK() const
        {
            if (Rex() < 1) {
                return 0.666667;}
            else {
                return 0.5;}
         }


        /*!
            * \brief Evaluates the boundary conditions for a Neumann boundary segment.
            *
            * For this method, the \a priVars parameter stores the mass flux
            * in normal direction of each component. Negative values mean influx.
            * \param globalPos The global position
            *
            * \note The units must be according to either using mole or mass fractions (mole/(m^2*s) or kg/(m^2*s)).
            */

        NumEqVector neumann(const Element& element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const ElementFluxVariablesCache& elemFluxVarsCache,
                            const SubControlVolumeFace& scvf) const
                            {
                                NumEqVector values(0.0);
                                const auto globalPos = scvf.ipGlobal();
                                const auto& volVars = elemVolVars[scvf.insideScvIdx()];

                                //Get the binary diffusion coeffcients of the gas phase
                                //TODO: we neglect the composition of the FF domain. However, we should consider the diffusion coefficient at the interface! --> Get the averaged diffusion coeffcient of both domains.

                                Scalar diffusionCoefficient = volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx);
                                Scalar diffusionCoefficientHDO = volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::gasPhaseIdx, FluidSystem::HDOIdx);
                                Scalar diffusionCoefficientH2O18 = volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::gasPhaseIdx, FluidSystem::H2O18Idx);

                                //Set resistance factor for boundary condition
                                Scalar rah = boundarylayerthickness()/diffusionCoefficient;

                                // Set kinetic fractionation factor either by Merlivat/Jouzel(1978) or by Stewart(1975) for boundary condition
                                // Calculate ratio restistance for kinetic fractionation factor after Merlivat/Jouzel(1978)
                                //                                 Scalar ratioResistance = 0.0;
                                //                                 if (Rex() < 1) { //laminar flow
                                //                                 ratioResistance = 1.0/0.4 * log (FrictionVelocity()*heightWind_ / 30.0 / kinematicVisocsity()) / (13.6*pow((kinematicVisocsity()/ diffusionCoefficient),nK()));}
                                //                                 else {
                                //                                 ratioResistance = (1.0/0.4*log (heightWind_ / surfaceRoughness_) - 5.0) /  ( 7.3*pow(Rex(), (1.0/4.0))*pow(kinematicVisocsity() /diffusionCoefficient,nK()));
                                //                                 }

                                //Set kinetic fractionation factor for HDO
                                //Merlivat/Jouzel (1978)
                                //                                 Scalar alpha_k_HDO = (pow((diffusionCoefficient/diffusionCoefficientHDO),nK()) + ratioResistance) / ( 1.0+ratioResistance);

                                //Stewart(1975)
                                //                                 Scalar nK_HDO = ((volVars.saturation(FluidSystem::liquidPhaseIdx)*volVars.porosity() - residualSaturation_)*0.5 +(saturatedWaterContent_- volVars.saturation(FluidSystem::liquidPhaseIdx)*volVars.porosity()))/ (saturatedWaterContent_ - residualSaturation_);
                                //
                                //                                 Scalar alpha_k_HDO =  pow((diffusionCoefficient/diffusionCoefficientHDO),nK_HDO);

                                //Set kinetic fractionation factor for H2O18
                                //Merlivat/Jouzel (1978)
                                //                                 Scalar alpha_k_H2O18 = (pow((diffusionCoefficient/diffusionCoefficientH2O18),nK()) + ratioResistance) / ( 1.0+ratioResistance);

                                //Stewart(1975)
                                //                                 Scalar nK_H2O18 = ((volVars.saturation(FluidSystem::liquidPhaseIdx)*volVars.porosity() - residualSaturation_)*0.5 +(saturatedWaterContent_- volVars.saturation(FluidSystem::liquidPhaseIdx)*volVars.porosity()))/ (saturatedWaterContent_ - residualSaturation_);
                                //                                 Scalar alpha_k_H2O18 =  pow((diffusionCoefficient/diffusionCoefficientH2O18),nK_H2O18);


                                if(globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_) // upper Neumann boundary condition
                                {
                                    // Isotopic evaporation flux
                                    //Set simpified boundary condition(only molecular diffusion) or acutal condition (kinetic fractionation considered)
                                    Scalar evaporationHDORateMole = volVars.molarDensity(FluidSystem::gasPhaseIdx)*(volVars.moleFraction(FluidSystem::gasPhaseIdx, FluidSystem::HDOIdx) - refMoleFracHDOVapor())/boundarylayerthickness()*diffusionCoefficientHDO; //For H2O18 Flux
                                    Scalar evaporationH2O18RateMole = volVars.molarDensity(FluidSystem::gasPhaseIdx)*(volVars.moleFraction(FluidSystem::gasPhaseIdx, FluidSystem::H2O18Idx) - refMoleFracH2O18Vapor())/boundarylayerthickness()*diffusionCoefficientH2O18; //For H2O18 Flux

                                    //                                   Scalar evaporationHDORateMole = volVars.molarDensity(FluidSystem::gasPhaseIdx)*(volVars.moleFraction(FluidSystem::gasPhaseIdx, FluidSystem::HDOIdx) - refMoleFracHDOVapor())/rah/alpha_k_HDO; //For H2O18 Flux
                                    //                                     Scalar evaporationH2O18RateMole = volVars.molarDensity(FluidSystem::gasPhaseIdx)*(volVars.moleFraction(FluidSystem::gasPhaseIdx, FluidSystem::H2O18Idx) - refMoleFracH2O18Vapor())/rah/alpha_k_H2O18; //For H2O18 Flux
                                    //
                                    //evporation flux of ordinary water
                                    Scalar evaporationRateMole =                                    volVars.molarDensity(FluidSystem::gasPhaseIdx)*(volVars.moleFraction(FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx) - refMoleFracVapor())/rah; //For H2O Flux



                                    values[Indices::conti0EqIdx] = evaporationRateMole;
                                    values[Indices::conti0EqIdx+1] = -evaporationRateMole;
                                    values[Indices::conti0EqIdx+2] = evaporationHDORateMole;
                                    values[Indices::conti0EqIdx+1] += -evaporationHDORateMole;
                                    values[Indices::conti0EqIdx+3] = evaporationH2O18RateMole;
                                    values[Indices::conti0EqIdx+1] += -evaporationH2O18RateMole;

                                    if (volVars.pressure(FluidSystem::gasPhaseIdx) - 1e5 > 0)
                                    {
                                        values[Indices::conti0EqIdx+1] = (volVars.pressure(FluidSystem::gasPhaseIdx) - 1e5)
                                        /(globalPos - fvGeometry.scv(scvf.insideScvIdx()).center()).two_norm()
                                        *volVars.mobility(FluidSystem::gasPhaseIdx)
                                        *this->spatialParams().permeabilityAtPos(globalPos)
                                        *volVars.molarDensity(FluidSystem::gasPhaseIdx) * volVars.moleFraction(FluidSystem::gasPhaseIdx, FluidSystem::AirIdx);

                                    }
                                    else
                                    {
                                        values[Indices::conti0EqIdx+1] = (volVars.pressure(FluidSystem::gasPhaseIdx) - 1e5)
                                        /(globalPos - fvGeometry.scv(scvf.insideScvIdx()).center()).two_norm()
                                        *volVars.mobility(FluidSystem::gasPhaseIdx)
                                        *this->spatialParams().permeabilityAtPos(globalPos)
                                        *volVars.molarDensity(FluidSystem::gasPhaseIdx) * refMoleFracAirVapor();
                                    }


                                    #if NONISOTHERMAL
                                    values[Indices::energyEqIdx] = H2O::gasEnthalpy(volVars.temperature(),volVars.pressure(FluidSystem::gasPhaseIdx)) * values[Indices::conti0EqIdx]*FluidSystem::molarMass(FluidSystem::H2OIdx);

                                    values[Indices::energyEqIdx] += HDO::gasEnthalpy(volVars.temperature(),volVars.pressure(FluidSystem::gasPhaseIdx)) * values[Indices::conti0EqIdx+2]*FluidSystem::molarMass(FluidSystem::HDOIdx);

                                    values[Indices::energyEqIdx] += H2O18::gasEnthalpy(volVars.temperature(),volVars.pressure(FluidSystem::gasPhaseIdx)) * values[Indices::conti0EqIdx+3]*FluidSystem::molarMass(FluidSystem::H2O18Idx);

                                    values[Indices::energyEqIdx] += Air::gasEnthalpy(refTemperature(),1e5) * values[Indices::conti0EqIdx+1]*FluidSystem::molarMass(FluidSystem::AirIdx);

                                    values[Indices::energyEqIdx] += Air::gasThermalConductivity(refTemperature(), 1)* (volVars.temperature() - refTemperature())/boundarylayerthickness();


                                    // get radiation from input data
                                    //values[Indices::energyEqIdx] += -1*evaluateData(radiationDataPM_, previousTime(), time());
                                    //
                                    // get calculated radiation
                                    //values[Indices::energyEqIdx] += -1*Radiation::radationEquilibrium(element, elemVolVars[scvf.insideScvIdx()], scvf,  refTemperature(), time());
                                    #endif
                                }

                                return values;
                        }


        // \}

    /*!
        * \name Volume terms
        */
    // \{

    /*!
        * \brief Evaluates the initial values for a control volume.
        *
        * \param globalPos The global position
        */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        return initial_(globalPos);
    }

    // \}

    private:
        /*!
         * \brief Evaluates the initial values for a control volume.
         *
         * The internal method for the initial condition
         *
         * \param globalPos The global position
         */
        PrimaryVariables initial_(const GlobalPosition &globalPos) const
        {
            PrimaryVariables priVars(0.0);
            priVars.setState(Indices::bothPhases);
            priVars[Indices::pressureIdx] = 1.0e5;
            Scalar temperature = this->spatialParams().temperatureAtPos(globalPos);

            //Set primary variables constant over soil column depth
            Scalar R_D_L = deltaD_*ref_D_/1000+ref_D_;
            Scalar R_O18_L = deltaO18_*ref_O18_/1000+ref_O18_;
            Scalar R_D = R_D_L*(exp(-((24844/(temperature*temperature))+(-76.248/temperature)+0.052612)));
            Scalar R_O18 = R_O18_L*(exp(-((1137/(temperature*temperature))+(-0.4156/temperature)-0.0020667)));
            Scalar vaporPressure = H2O::vaporPressure(temperature);
            priVars[Indices::conti0EqIdx+2] = R_D*vaporPressure/1e5;
            priVars[Indices::conti0EqIdx+3] = R_O18*vaporPressure/1e5;
            priVars[Indices::switchIdx] = saturation_;
            #if NONISOTHERMAL
            priVars[Indices::temperatureIdx] = temperature;
            #endif
            //set inital saturation by using the material law

            //              if (globalPos[0] < heightWT_ - eps_){
            //             using MaterialLaw = typename ParentType::SpatialParams::MaterialLaw;
            //             Scalar pc = 1.0* FluidSystem::H2O::liquidDensity((273.15 +15.78), 1.0e5)*this->spatialParams().gravity(globalPos)[dimWorld-1]*(height_);
            //             // MaterialLaw get sw and we use sw as inital
            //             const Scalar sw = MaterialLaw::sw(this->spatialParams().materialLawParamsAtPos(globalPos), pc);
            //
            //             priVars[Indices::switchIdx] = sw;}
            //
            //             else {
            //             using MaterialLaw = typename ParentType::SpatialParams::MaterialLaw;
            //             Scalar pc = 1.0* FluidSystem::H2O::liquidDensity((273.15 +15.78), 1.0e5)*this->spatialParams().gravity(globalPos)[dimWorld-1]*(heightWK_-globalPos[dimWorld-1]);
            //
            //             // MaterialLaw get sw and we use sw as inital
            //             const Scalar sw = MaterialLaw::sw(this->spatialParams().materialLawParamsAtPos(globalPos), pc);
            //             priVars[Indices::switchIdx] = sw;}
            //
            //             //set initally different isotopic compostions for certain depths
            // //
            //                                     if (globalPos[0] < 0.2 - eps_)
            //             priVars[Indices::conti0EqIdx+2] =  (-52.49*ref_D_/1000+ref_D_)*(exp(-((24844.0/((273.15 + 15.347)*(273.15 + 15.347))+((-76.248)/(273.15 + 15.347)) + 0.052612))))*H2O::vaporPressure(273.15 + 15.347)/1e5;
            //             else if (globalPos[0] > 0.2 + eps_ && globalPos[0] < 0.4 -eps_)
            //                 priVars[Indices::conti0EqIdx+2] =   (-53.51*ref_D_/1000+ref_D_)*(exp(-((24844.0/((273.15 + 15.337)*(273.15 + 15.337))+((-76.248)/(273.15 + 15.337)) + 0.052612))))*H2O::vaporPressure(273.15 + 15.337)/1e5;
            //              else if (globalPos[0] > 0.4 + eps_ && globalPos[0] < 0.5 -eps_)
            //             priVars[Indices::conti0EqIdx+2] =   (-52.07*ref_D_/1000+ref_D_)*(exp(-((24844.0/((273.15 + 14.996)*(273.15 + 14.996))+((-76.248)/(273.15 + 14.996)) + 0.052612))))*H2O::vaporPressure(273.15 + 14.996)/1e5;
            //              else if (globalPos[0] > 0.5 + eps_ && globalPos[0] < 0.53 -eps_)
            //             priVars[Indices::conti0EqIdx+2] =   (-52.39*ref_D_/1000+ref_D_)*(exp(-((24844.0/((273.15 + 15.061)*(273.15 + 15.061))+((-76.248)/(273.15 + 15.061)) + 0.052612))))*H2O::vaporPressure(273.15 + 15.061)/1e5;
            //             else if (globalPos[0] > 0.53 + eps_ && globalPos[0] < 0.55 -eps_)
            //             priVars[Indices::conti0EqIdx+2] =  (-52.09*ref_D_/1000+ref_D_)*(exp(-((24844.0/((273.15 + 14.965)*(273.15 + 14.965))+((-76.248)/(273.15 + 14.965)) + 0.052612))))*H2O::vaporPressure(273.15 + 14.965)/1e5;
            //             else if (globalPos[0] > 0.55 + eps_ && globalPos[0] < 0.57 -eps_)
            //             priVars[Indices::conti0EqIdx+2] =   (-53.04*ref_D_/1000+ref_D_)*(exp(-((24844.0/((273.15 + 15.164)*(273.15 + 15.164))+((-76.248)/(273.15 + 15.164)) + 0.052612))))*H2O::vaporPressure(273.15 + 15.164)/1e5;
            //              else if (globalPos[0] > 0.57 + eps_ && globalPos[0] < 0.59 -eps_)
            //             priVars[Indices::conti0EqIdx+2] =   (-53.38*ref_D_/1000+ref_D_)*(exp(-((24844.0/((273.15 + 15.259)*(273.15 + 15.259))+((-76.248)/(273.15 + 15.259)) + 0.052612))))*H2O::vaporPressure(273.15 + 15.259)/1e5;
            //             else
            //             priVars[Indices::conti0EqIdx+2] =  (-53.17*ref_D_/1000+ref_D_)*(exp(-((24844.0/((273.15 + 15.78)*(273.15 + 15.78))+((-76.248)/(273.15 + 15.78)) + 0.052612))))*H2O::vaporPressure(273.15 + 15.78)/1e5;
            //
            //                         if (globalPos[0] < 0.2 - eps_)
            //             priVars[Indices::conti0EqIdx+3] =  (-7.98*ref_O18_/1000+ref_O18_)*(exp(-((1137.0/((273.15 + 15.347)*(273.15 + 15.347))+((-0.4156)/(273.15 + 15.347)) -0.0020667))))*H2O::vaporPressure(273.15 + 15.347)/1e5;
            //             else if (globalPos[0] > 0.2 + eps_ && globalPos[0] < 0.4 -eps_)
            //             priVars[Indices::conti0EqIdx+3] =  (-8.05*ref_O18_/1000+ref_O18_)*(exp(-((1137.0/((273.15 + 15.337)*(273.15 + 15.337))+((-0.4156)/(273.15 + 15.337)) -0.0020667))))*H2O::vaporPressure(273.15 + 15.337)/1e5;
            //              else if (globalPos[0] > 0.4 + eps_ && globalPos[0] < 0.5 -eps_)
            //             priVars[Indices::conti0EqIdx+3] =  (-7.91*ref_O18_/1000+ref_O18_)*(exp(-((1137.0/((273.15 + 14.996)*(273.15 + 14.996))+((-0.4156)/(273.15 + 14.996)) -0.0020667))))*H2O::vaporPressure(273.15 + 14.996)/1e5;
            //              else if (globalPos[0] > 0.5 + eps_ && globalPos[0] < 0.53 -eps_)
            //             priVars[Indices::conti0EqIdx+3] =  (-7.92*ref_O18_/1000+ref_O18_)*(exp(-((1137.0/((273.15 + 15.061)*(273.15 + 15.061))+((-0.4156)/(273.15 + 15.061)) -0.0020667))))*H2O::vaporPressure(273.15 + 15.061)/1e5;
            //             else if (globalPos[0] > 0.53 + eps_ && globalPos[0] < 0.55 -eps_)
            //             priVars[Indices::conti0EqIdx+3] =  (-7.83*ref_O18_/1000+ref_O18_)*(exp(-((1137.0/((273.15 + 14.965)*(273.15 + 14.965))+((-0.4156)/(273.15 + 14.965)) -0.0020667))))*H2O::vaporPressure(273.15 + 14.965)/1e5;
            //             else if (globalPos[0] > 0.55 + eps_ && globalPos[0] < 0.57 -eps_)
            //             priVars[Indices::conti0EqIdx+3] =  (-7.75*ref_O18_/1000+ref_O18_)*(exp(-((1137.0/((273.15 + 15.164)*(273.15 + 15.164))+((-0.4156)/(273.15 + 15.164)) -0.0020667))))*H2O::vaporPressure(273.15 + 15.164)/1e5;
            //              else if (globalPos[0] > 0.57 + eps_ && globalPos[0] < 0.59 -eps_)
            //             priVars[Indices::conti0EqIdx+3] =  (-7.88*ref_O18_/1000+ref_O18_)*(exp(-((1137.0/((273.15 + 15.259)*(273.15 + 15.259))+((-0.4156)/(273.15 + 15.259)) -0.0020667))))*H2O::vaporPressure(273.15 + 15.259)/1e5;
            //             else
            //             priVars[Indices::conti0EqIdx+3] =  (-7.76*ref_O18_/1000+ref_O18_)*(exp(-((1137.0/((273.15 + 15.78)*(273.15 + 15.78))+((-0.4156)/(273.15 + 15.78)) -0.0020667))))*H2O::vaporPressure(273.15 + 15.78)/1e5;
            //
            //
            // //              set initally different temperatures for certain depths
            //
            // #if NONISOTHERMAL
            //
            //                     if (globalPos[0] < 0.2 - eps_)
            //             priVars[Indices::temperatureIdx] =  273.15 + 15.347;
            //             else if (globalPos[0] > 0.2 + eps_ && globalPos[0] < 0.4 -eps_)
            //             priVars[Indices::temperatureIdx] =   273.15 + 15.337;
            //              else if (globalPos[0] > 0.4 + eps_ && globalPos[0] < 0.5 -eps_)
            //             priVars[Indices::temperatureIdx] =   273.15 + 14.996;
            //              else if (globalPos[0] > 0.5 + eps_ && globalPos[0] < 0.53 -eps_)
            //             priVars[Indices::temperatureIdx] =   273.15 + 15.061;
            //             else if (globalPos[0] > 0.53 + eps_ && globalPos[0] < 0.55 -eps_)
            //             priVars[Indices::temperatureIdx] =   273.15 + 14.965;
            //             else if (globalPos[0] > 0.55 + eps_ && globalPos[0] < 0.57 -eps_)
            //             priVars[Indices::temperatureIdx] =   273.15 + 15.164;
            //              else if (globalPos[0] > 0.57 + eps_ && globalPos[0] < 0.59 -eps_)
            //             priVars[Indices::temperatureIdx] =   273.15 + 15.259;
            //             else
            //             priVars[Indices::temperatureIdx] =   273.15 +15.78;
            // #endif


            return priVars;
        }


        static constexpr Scalar eps_ = 1e-6;
        std::string name_ ;
        std::string outputname_ ;

        Scalar time_;
        Scalar previousTime_;
        Scalar distance_;// [m]  distance downstream from start of the boundary layer; free varaiable
        Scalar heightWind_ = 1.8; // [m] Measurement height of wind speed
        Scalar surfaceRoughness_= 0.0005; // [m]  mean particle size of sand FH31

        //soil specific values based on Stingaciu(2009)
        Scalar residualSaturation_ = 0.02;  // residual water saturation[cm3/cm3]
        Scalar saturatedWaterContent_ = 0.321; // saturated water saturation [cm3/cm3]

        //reference values of the delta notation
        Scalar ref_D_ = 155.76e-6;
        Scalar ref_O18_ = 2005.2e-6;

        Scalar airTemperature_;
        Scalar relativeHumidity_;
        Scalar atmosphericDeltaD_;
        Scalar atmosphericDeltaO18_;
        Scalar windSpeed_;
        Scalar boundaryLayerThickness_;
        Scalar pressure_;
        Scalar saturation_;
        Scalar deltaD_;
        Scalar deltaO18_;
        Scalar height_;
        Scalar heightWK_;
        Scalar heightWT_;

        std::string temperatureDataFilePM_;
        std::vector<double> temperatureDataPM_[2];
        std::string relativeHumidityDataFilePM_;
        std::vector<double> relativeHumidityDataPM_[2];
        std::string atmosphericHDODataFilePM_;
        std::vector<double> atmosphericHDODataPM_[2];
        std::string atmosphericH2O18DataFilePM_;
        std::vector<double> atmosphericH2O18DataPM_[2];

        std::string radiationDataFilePM_;
        std::vector<double> radiationDataPM_[2];

        Dumux::GnuplotInterface<double> gnuplot_;
        Dumux::GnuplotInterface<double> gnuplot2_;
        Dumux::GnuplotInterface<double> gnuplot3_;
        Dumux::GnuplotInterface<double> gnuplot4_;
        Dumux::GnuplotInterface<double> gnuplot5_;

        std::vector<double> x_;
        std::vector<double> y_;
        std::vector<double> y2_;
        std::vector<double> y3_;
        std::vector<double> y4_;
        std::vector<double> y5_;
        std::vector<double> y6_;
        std::vector<double> y7_;
        std::vector<double> y8_;
        std::vector<double> y9_;

        bool plotOutput_;


    };

} // end namespace Dumux

#endif
