install(FILES
        iofields.hh
DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/)

add_subdirectory(io)
add_subdirectory(material)
add_subdirectory(radiation)
add_subdirectory(volumevariables)
