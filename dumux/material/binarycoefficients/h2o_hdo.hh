// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Binarycoefficients
 * \brief Binary coefficients for water and HDO.
 */
#ifndef DUMUX_BINARY_COEFF_H2O_HDO_HH
#define DUMUX_BINARY_COEFF_H2O_HDO_HH

#include <cmath>

namespace Dumux {
namespace BinaryCoeff {

/*!
* \ingroup Binarycoefficients
* \brief Binary coefficients for water and HDO.
*/
class H2O_HDO
{
public:
    /*!
    * \brief Henry coefficient \f$\mathrm{[Pa]}\f$  for air in liquid water.
    * \param temperature the temperature \f$\mathrm{[K]}\f$
    *
    * Henry coefficient See:
    * Stefan Finsterle (1993, page 33 Formula (2.9)) \cite finsterle1993 <BR>
    * (fitted to data from Tchobanoglous & Schroeder, 1985 \cite tchobanoglous1985 )
    */
    template <class Scalar>
    static Scalar henry(Scalar temperature)
    {
        DUNE_THROW(Dune::NotImplemented,
                    "H2O_HDOBinarycoefficient::henry()");

    }

    /*!
    * \brief Binary diffusion coefficient \f$\mathrm{[m^2/s]}\f$ for molecular water and air
    *
    * \param temperature the temperature \f$\mathrm{[K]}\f$
    * \param pressure the phase pressure \f$\mathrm{[Pa]}\f$
    * Vargaftik: Tables on the thermophysical properties of liquids and gases.
    * John Wiley & Sons, New York, 1975. \cite vargaftik1975 <BR>
    * Walker, Sabey, Hampton: Studies of heat transfer and water migration in soils.
    * Dep. of Agricultural and Chemical Engineering, Colorado State University,
    * Fort Collins, 1981. \cite walker1981
    */
    template <class Scalar>
    static Scalar gasDiffCoeff(Scalar temperature, Scalar pressure)
    {

        DUNE_THROW(Dune::NotImplemented,
                    "H2O_HDOBinarycoefficient::gasDiffCoeff()");
    }

    /*!
    * Lacking better data on water-air diffusion in liquids, we use at the
    * moment the diffusion coefficient of the air's main component nitrogen!!
    * \brief Diffusion coefficient \f$\mathrm{[m^2/s]}\f$ for molecular nitrogen in liquid water.
    *
    * \param temperature the temperature \f$\mathrm{[K]}\f$
    * \param pressure the phase pressure \f$\mathrm{[Pa]}\f$
    *
    * The empirical equations for estimating the diffusion coefficient in
    * infinite solution which are presented in Reid, 1987 all show a
    * linear dependency on temperature. We thus simply scale the
    * experimentally obtained diffusion coefficient of Ferrell and
    * Himmelblau by the temperature.
    *
    * See:
    * R. Reid et al. (1987, pp. 599) \cite reid1987 <BR>
    * R. Ferrell, D. Himmelblau (1967, pp. 111-115) \cite ferrell1967
    */
    template <class Scalar>
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    {
        using std::pow;
        using std::exp;
        const Scalar a = 0.9833; //[-] Quelle: Braud (2005)
        Scalar Dliq = a*pow(10,-9)*exp((-535400/pow(temperature,2))+(1393.3/temperature)+ 2.1876);
        return Dliq;

    }
};

} // end namespace BinaryCoeff
} // end namespace Dumux

#endif
